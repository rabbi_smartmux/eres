from rest_framework import serializers

from apps.campaigns.models import Campaign

class CampaignsSerializer(serializers.ModelSerializer):
    publisher__code = serializers.CharField(source='publisher.code',read_only=True,allow_blank=True)
    # click_url = serializers.CharField(source='obj.campaigntargets.landing_url',read_only=True,allow_blank=True)
    click_url = serializers.SerializerMethodField(read_only=True)

    def get_click_url(self,obj):
        print(obj)
        return obj.campaigntargets.landing_url
    class Meta:
        model = Campaign
        fields = ('name','code','publisher','publisher__code','campaign_type','position_type','min_bid_price','adv_bid_price','daily_goal','logo_placement','click_url')
