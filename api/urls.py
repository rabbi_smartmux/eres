from django.urls import path
import os


from api.views import AdServe,AdServePopup
from dsp.urls import router2

# router2
urlpatterns = [
    path('ads/serve', AdServe.as_view(), name='ads_serve'),
    path('popup-ads/serve', AdServePopup.as_view(), name='ads_popup_serve'),
]
# if not os.name == 'nt':
