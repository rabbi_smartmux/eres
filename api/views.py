import datetime
import json
import random
from random import randrange
import time

from django.core import serializers
from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.accounts.models import Company
from apps.helpers.views import get_user_agent_details, get_ip2location_data
from pymongo import MongoClient

class AdServe(APIView):
    permission_classes = ()

    def get(self, requests):
        print(dict(self.request.query_params.lists()))
        code = self.request.query_params.get('code')
        w = self.request.query_params.get('w')
        h = self.request.query_params.get('h')
        url = self.request.query_params.get('url')
        ip = self.request.query_params.get('ip')
        uag = self.request.query_params.get('uag')

        return Response(dict(self.request.query_params.lists()))


class AdServePopup(APIView):
    permission_classes = ()

    def get(self, requests):
        request_meta = self.request.META
        meta_data = self.request_meta_serialize(request_meta)

        publisher_code = self.request.query_params.get('code')
        width = self.request.query_params.get('width', 0)
        height = self.request.query_params.get('height', 0)
        url = self.request.query_params.get('url')
        ip = meta_data.get('HTTP_X_REAL_IP')
        uag = meta_data.get('HTTP_USER_AGENT')
        type = self.request.query_params.get('type')
        cookie = self.request.query_params.get('_ga')
        video_time = self.request.query_params.get('video_time', 0)
        details = self.request.query_params
        request_type = 'Popup'

        creatives = Creatives.objects.filter(type=1, campaign__publisher__code=publisher_code,
                                             campaign__position_type=3, status=1,
                                             campaign__status=1).order_by('?').first()
        print(creatives, publisher_code)
        print(uag)

        publisher = Company.objects.filter(code=publisher_code).first()

        adRequest = AdRequest.objects.create(
            ip=ip,
            uag=uag,
            type=type,
            cookie=cookie,
            details=details,
            url=url,
            height=height,
            width=width,
            publisher_code=publisher_code,
            publisher=publisher,
            video_time=video_time,
            request_type=request_type,
            request_meta=meta_data
            # ua_id=get_user_agent_details(uag).get('id', None),
            # ip2location_id=get_ip2location_data(ip).get('id', None)
        )

        data = [
            # {
            #     'popup': False,
            #     'html': '',
            #     'loadUrl': ''
            # },
            # {
            #     'popup': True,
            #     'html': '<iframe id="HTML5click" src="https://s0.2mdn.net/dfp/366178/160891738/1690953328361/index.html" width="660px" height="440px" scrolling="no" frameborder="0"></iframe>',
            #     'loadUrl': 'https://s0.2mdn.net/dfp/366178/160891738/1690953328361/index.html'
            # },
            # {
            #     'popup': True,
            #     'html': '<iframe id="HTML5click" src="https://ms.purplepatch.online/2023/taaza/" width="300px" height="250px" scrolling="no" frameborder="0"></iframe>',
            #     'loadUrl': 'https://ms.purplepatch.online/2023/taaza/'
            # },
            {
                'popup': True,
                'html': '<img id="HTML5click" src="https://rh.purplepatch.online/assets/DFG-placeholder-1.jpg" width="660px" height="440px"/>',
                'loadUrl': 'https://4.img-dpreview.com/files/p/E~TS590x0~articles/3925134721/0266554465.jpeg'
            },
            {
                'popup': True,
                'html': '<img id="HTML5click" src="https://rh.purplepatch.online/assets/DFG-placeholder-1.jpg"  width="660px" height="440px" />',
                'loadUrl': 'https://4.img-dpreview.com/files/p/TS600x600~sample_galleries/3002635523/4971879462.jpg'
            }
        ]

        print(creatives)

        if (url.find('https://www.rabbitholebd.com/v3/home') == 0):
            if creatives:
                CLICK_URL = f"'https://ads.purplepatch.online/clients/api/click?rid={adRequest.id}','_blank'"
                data = {
                    'popup': True,
                    'html': f'<div id="pp_popup_rh" onclick="window.open({CLICK_URL});"><img id="HTML5click" src="https://ads.purplepatch.online{creatives.file.url}"  width="660px" height="440px" /></div>',
                    'loadUrl': 'https://ads.purplepatch.online' + creatives.file.url
                }
                adRequest.impression = True
                adRequest.creative = creatives
                adRequest.save()
            else:
                data = {
                    'popup': False,
                    'html': '',
                    'loadUrl': ''
                }

        else:
            data = {
                'popup': False,
                'html': '',
                'loadUrl': ''
            }
        # time.sleep(60)  # Sleep for 60 seconds
        # time.sleep(60)

        return Response(data)

    def post(self, requests):
        print(self.request.data)
        return Response(self.request.data)

    def request_meta_serialize(self, meta):

        data = {}
        for item in list(meta):
            # print(meta[item],type(meta[item]),type(meta[item]) in [str,bool])
            if type(meta[item]) in [str, bool, int, float]:
                data[item] = meta[item]

        return data


