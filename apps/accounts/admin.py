from django.contrib import admin

# Register your models here.
from apps.accounts.models import Profile, CompanyType, Company, BloodBank, HealthProblem, LifeStyle, Patient, \
    Appointment, Zilla, Upazila, Union, Ward


class CompanyTypeAdmin(admin.ModelAdmin):
    list_display = ["name", "description"]
admin.site.register(CompanyType,CompanyTypeAdmin)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name", "code","type"]
admin.site.register(Company,CompanyAdmin)

class ProfileAdmin(admin.ModelAdmin):
    list_display = ["user", "mobile", "company", "location"]
admin.site.register(Profile,ProfileAdmin)

class BloodBankAdmin(admin.ModelAdmin):
    list_display = ["donor", "collection_date", "expiry_date"]
admin.site.register(BloodBank,BloodBankAdmin)

class HealthProblemAdmin(admin.ModelAdmin):
    list_display = ["name"]
admin.site.register(HealthProblem,HealthProblemAdmin)

class LifeStyleAdmin(admin.ModelAdmin):
    list_display = ["name"]
admin.site.register(LifeStyle,LifeStyleAdmin)

class PatientAdmin(admin.ModelAdmin):
    list_display = ["patient_name"]
admin.site.register(Patient,PatientAdmin)

class AppointmentAdmin(admin.ModelAdmin):
    list_display = ["patient"]
admin.site.register(Appointment,AppointmentAdmin)

class ZillaAdmin(admin.ModelAdmin):
    list_display = ["zilla"]
admin.site.register(Zilla,ZillaAdmin)

class UpazilaAdmin(admin.ModelAdmin):
    list_display = ["zilla","upazila"]
admin.site.register(Upazila,UpazilaAdmin)

class UnionAdmin(admin.ModelAdmin):
    list_display = ["union","upazila"]
admin.site.register(Union,UnionAdmin)

class WardAdmin(admin.ModelAdmin):
    list_display = ["union","ward"]
admin.site.register(Ward,WardAdmin)




