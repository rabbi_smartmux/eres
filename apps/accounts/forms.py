from betterforms.multiform import MultiModelForm
from crispy_forms.bootstrap import PrependedText, PrependedAppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit, Button, Div
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import Group, Permission, User
from django.core.exceptions import ValidationError
from django.db.models import Q
from django import forms
from django.forms import ModelForm, fields, ModelChoiceField, ModelMultipleChoiceField, DateField, DateInput
from django.utils.safestring import mark_safe

from apps.helpers.utils import LIFE_STYLE, HEALTH_PPROBLEM
from tabular_permissions.widgets import TabularPermissionsWidget

from apps.accounts.models import Company, GroupProfile, CompanyType, Staff, Patient, Appointment, BloodBank, Donor, \
    LifeStyle, HealthProblem, PregnancyCycle, StaffWorkingLog, Zilla, Upazila, Union, Ward
from apps.helpers.forms import TypeModelChoiceField, TypeModelChoiceFieldZilla, \
    TypeModelChoiceFieldUpazila, TypeModelChoiceFieldUnion, TypeModelChoiceFieldWard


class CustomAuthenticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Row(
                Column(
                    PrependedText('username', '', placeholder="Username"),
                    css_class='form-group col-md-12 mb-0'
                ),
                css_class='form-row'
            ),
            Row(
                Column(
                    PrependedAppendedText('password', '', mark_safe('<i class="fas fa-eye-slash password_view"></i>'),
                                          placeholder="Password"),
                    css_class='form-group col-md-12 mb-0'
                ),
                css_class='form-row'
            ),
            Submit('submit', 'Login', css_class='btn-lg btn-block submit_button custom-placeholder')
        )


class GroupForm(ModelForm):
    # company = TypeModelChoiceField(queryset=Company.objects.filter(status=True))
    class Meta:
        model = Group
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        user = self.request.user
        super(GroupForm, self).__init__(*args, **kwargs)
        if self.initial:
            # self.initial['name'] = self.initial['name'].split('-')[1]
            # self.initial['company'].initial = 123
            pass
        if user.is_superuser:
            permission = Permission.objects.all()
        else:
            permission = Permission.objects.filter(Q(group__user=user) | Q(user=user)).distinct()
        self.fields['permissions'].widget = TabularPermissionsWidget(verbose_name='perm', is_stacked=False,permission=permission)

    # def clean_name(self):
    #     name = self.cleaned_data['name']
    #     except_list = ['www', 'admin']
    #     if '-' in name:
    #         raise ValidationError('Hyphen/Dash is not allowed', code='invalid')
    #     sdomain = self.request.sdomain.sdomain.split('.')[0]
    #     if sdomain in except_list:
    #         sdomain = 'bcbd'
    #     if sdomain in name:
    #         data = name
    #     else:
    #         data = sdomain + "-" + name
    #     return data

class GroupProfileForm(ModelForm):
    class Meta:
        model = GroupProfile
        exclude = ['name', 'created_by', 'updated_by', 'created_at', 'updated_at','description']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(GroupProfileForm, self).__init__(*args, **kwargs)
        self.fields['can_view_dashboard'].widget.attrs['class'] = 'checkbox'
        if not self.request.user.is_staff:
            self.fields['company'].widget = self.fields['company'].hidden_widget()
            self.fields['company'].initial = self.request.user.profile.company.id

        if self.initial:
            # self.initial['name'] = self.initial['name'].split('-')[1]
            # self.initial['company'].initial = 123
            pass
        # self.fields['can_see_all_application'].widget.attrs['class'] = 'checkbox'


class GroupMultiForm(MultiModelForm):
    form_classes = {
        'group': GroupForm,
        'profile': GroupProfileForm,
    }

    def get_form_args_kwargs(self, key, args, kwargs):
        fargs, fkwargs = super(GroupMultiForm, self).get_form_args_kwargs(key, args, kwargs)
        fkwargs.update({'request': kwargs.get('request')})
        return fargs, fkwargs

class GroupEditMultiForm(MultiModelForm):
    form_classes = {
        'group': GroupForm,
        'profile': GroupProfileForm,
    }

    def get_form_args_kwargs(self, key, args, kwargs):
        fargs, fkwargs = super(GroupEditMultiForm, self).get_form_args_kwargs(key, args, kwargs)
        fkwargs.update({'request': kwargs.get('request')})
        return fargs, fkwargs



class UserForm(UserCreationForm):
    first_name = fields.CharField(label='Full Name',required=True)
    username = fields.EmailField(label='Email Address')

    groups = ModelMultipleChoiceField(queryset=Group.objects.all())

    zilla = forms.ModelChoiceField(queryset=Zilla.objects.all(), required=False)
    upazila = forms.ModelChoiceField(queryset=Upazila.objects.all(), required=False)
    union = forms.ModelChoiceField(queryset=Union.objects.all(), required=False)
    ward = forms.ModelChoiceField(queryset=Ward.objects.all(), required=False)

    class Meta:
        model = User
        fields = ('first_name', 'username', 'password1', 'password2', 'groups')



class CompanyForm(ModelForm):
    type = ModelChoiceField(queryset=CompanyType.objects.all(),required=True, empty_label=None)
    class Meta:
        model = Company
        fields = ('name','code','type','status','email','website','address','phone')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'company_form'
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column(PrependedText('name', '', placeholder="Hospital Name"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('type', '', placeholder="Hospital Type"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('email', '', placeholder="Hospital Email"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('website', '', placeholder="Hospital Website URL"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('phone', '', placeholder="Hospital Phone"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('address', '', placeholder="Hospital Address"), css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('status', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )

class DateInput(DateInput):
    input_type = 'date'

class StaffForm(ModelForm):
    joining_date = DateField(required=True, widget=DateInput)
    class Meta:
        model = Staff
        fields = ('first_name','last_name','status','email','address','phone','joining_date')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'staff_form'
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column(PrependedText('first_name', '', placeholder="First Name"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('last_name', '', placeholder="Last Name"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('email', '', placeholder="Company Email"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('phone', '', placeholder="Company Phone"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('address', '', placeholder="Company Address"), css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('joining_date', css_class='form-group col-md-6 mb-0'),
                Column('status', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )


class StaffWorkingLogForm(ModelForm):
    class Meta:
        model = StaffWorkingLog
        fields = ('agency','user','patient','comment')

    def __init__(self, *args, **kwargs):
        request = kwargs.pop('request')
        request_user = request.user
        super(StaffWorkingLogForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_id = 'staffworkinglog_form'

        # Hide the 'user' field
        self.fields['user'].widget = forms.HiddenInput()
        self.fields['user'].initial = request_user.id

        if request_user.is_superuser or request_user.is_staff:
            self.fields['agency'].queryset = Company.objects.filter(type=2)
        elif request_user.profile.company.type_id == 2:
            self.fields['agency'].widget = self.fields['agency'].hidden_widget()

        self.helper.layout = Layout(
            Row(
                Column(PrependedText('patient', '', placeholder="Last Name"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('comment', '', placeholder="Comment"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )





class PatientForm(ModelForm):
    patient_age = DateField(required=True, widget=DateInput)
    husband_age = DateField(required=True, widget=DateInput)

    zilla = TypeModelChoiceFieldZilla(queryset=Zilla.objects.all(),required=True)
    upazila = TypeModelChoiceFieldUpazila(queryset=Upazila.objects.none(), required=True)
    union = TypeModelChoiceFieldUnion(queryset=Union.objects.none(), required=True)
    ward = TypeModelChoiceFieldWard(queryset=Ward.objects.none(), required=True)


    class Meta:
        model = Patient
        fields = ('registration_no','agency','user','patient_name','husband_name','patient_phone','husband_phone','couple_number','nid_number','road','ward','union','upazila','zilla','patient_age','husband_age','patient_blood_group','husband_blood_group','status')

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request").user
        super(PatientForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'patient_form'
        self.fields['user'].widget = forms.HiddenInput()
        self.fields['user'].initial = request_user.id
        if request_user.is_superuser or request_user.is_staff:
            self.fields['agency'].queryset = Company.objects.filter(type=2)
        elif request_user.profile.company.type_id==2:
            self.fields['agency'].initial = request_user.profile.company
            self.fields['agency'].widget = self.fields['agency'].hidden_widget()
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column(PrependedText('registration_no', '', placeholder="Registration Number"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('couple_number', '', placeholder="Couple Number"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('patient_name', '', placeholder="Patient Name"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('husband_name', '', placeholder="Husband Name"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('patient_phone', '', placeholder="Patient Phone"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('husband_phone', '', placeholder="Husband Phone"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('nid_number', '', placeholder="Patient NID"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('zilla', '', placeholder="Zilla"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('upazila', '', placeholder="Upazila"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('union', '', placeholder="Union"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('ward', '', placeholder="Ward"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('road', '', placeholder="Road"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),

            Row(
                Column('patient_age', css_class='form-group col-md-6 mb-0'),
                Column('husband_age', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('patient_blood_group', '', placeholder="patient_blood_group"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('husband_blood_group', '', placeholder="husband_blood_group"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('status', css_class='form-group col-md-6 mb-0'),
                Column('agency', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )


class CompanyFormByType(ModelForm):
    # type = ModelChoiceField(queryset=CompanyType.objects.all(),required=True, empty_label=None)
    class Meta:
        model = Company
        fields = ('name','code','type','status','email','website','address','phone')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'company_form'
        # self.helper.add_input(Submit('submit', 'Save person'))
        self.fields['type'].widget = self.fields['type'].hidden_widget()

        if self.initial:
            self.fields['type'].initial = self.initial.get('type')

        self.helper.layout = Layout(
            Row(
                Column(PrependedText('name', '', placeholder="Company Name"), css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Div('type'),
            Row(
                Column(PrependedText('email', '', placeholder="Company Email"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('website', '', placeholder="Company Website URL"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('phone', '', placeholder="Company Phone"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('address', '', placeholder="Company Address"), css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('status', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )


class AppointmentForm(ModelForm):
    next_appointment_date = DateField(required=True, widget=DateInput)
    visited_date = DateField(required=True, widget=DateInput)
    class Meta:
        model = Appointment
        fields = ('preeclampsia','iud','placema_location','next_appointment_date','agency','patient','comment','prescription','status','visited_date','bp','pulse','rbs','fundal_height')

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request").user
        super(AppointmentForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'appointment_form'
        if request_user.is_superuser or request_user.is_staff:
            self.fields['agency'].queryset = Company.objects.filter(type=2)
        elif request_user.profile.company.type_id==2:
            self.fields['agency'].widget = self.fields['agency'].hidden_widget()
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column('patient', css_class='form-group col-md-6 mb-0'),
                Column('visited_date', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('bp', '', placeholder="BP"),
                       css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('pulse', '', placeholder="Pulse"),
                       css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('rbs', '', placeholder="RBS"),
                       css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('fundal_height', '', placeholder="Fundal Height"),
                       css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('prescription', '', placeholder="prescription"),
                       css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('comment', '', placeholder="comment"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('next_appointment_date', css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('preeclampsia', '', placeholder="preeclampsia"), css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column(PrependedText('placema_location', '', placeholder="placema_location"),
                       css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('iud', '', placeholder="iud"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('status', css_class='form-group col-md-6 mb-0'),
            ),
            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )


class PregnancyCycleForm(ModelForm):
    lmp = DateField(required=True, widget=DateInput)
    edd = DateField(required=True, widget=DateInput)
    delivery_complete_date = DateField(required=False, widget=DateInput)
    life_style = ModelMultipleChoiceField(queryset=LifeStyle.objects.all(),required=False,widget=forms.CheckboxSelectMultiple)
    health_problem = ModelMultipleChoiceField(queryset=HealthProblem.objects.all(),required=False,widget=forms.CheckboxSelectMultiple)
    class Meta:
        model = PregnancyCycle
        fields = ('birth_type','delivery_complete_date','delivery_type','agency','patient','health_problem','life_style','period_off_time','pregnancy_number','alive_children_number','last_children_age','normal_delivery_number','csec_delivery_number','mr_number','monthly_income',
                'delivery_center','tt_dose_number', 'after_delivery_plan', 'complications', 'lmp', 'edd', 'status')

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request").user
        super(PregnancyCycleForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'pregnancycycle_form'
        if request_user.is_superuser or request_user.is_staff:
            self.fields['agency'].queryset = Company.objects.filter(type=2)
        elif request_user.profile.company.type_id==2:
            self.fields['agency'].widget = self.fields['agency'].hidden_widget()
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column('patient', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('period_off_time', '', placeholder="period_off_time"), css_class='form-group col-md-3 mb-0'),
                Column(PrependedText('pregnancy_number', '', placeholder="pregnancy_number"), css_class='form-group col-md-3 mb-0'),
                Column(PrependedText('alive_children_number', '', placeholder="alive_children_number"), css_class='form-group col-md-3 mb-0'),
                Column(PrependedText('last_children_age', '', placeholder="last_children_age"), css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('normal_delivery_number', '', placeholder="normal_delivery_number"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('csec_delivery_number', '', placeholder="csec_delivery_number"), css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('mr_number', '', placeholder="mr_number"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('monthly_income', '', placeholder="monthly_income"), css_class='form-group col-md-4 mb-0'),
                Column('delivery_center', css_class='form-group col-md-4 mb-0'),
                Column(PrependedText('tt_dose_number', '', placeholder="tt_dose_number"), css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column(PrependedText('after_delivery_plan', '', placeholder="after_delivery_plan"), css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('complications', '', placeholder="complications"), css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('lmp', css_class='form-group col-md-4 mb-0'),
                Column('edd', css_class='form-group col-md-4 mb-0'),
                Column('delivery_complete_date', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('birth_type', css_class='form-group col-md-4 mb-0'),
                Column('delivery_type', css_class='form-group col-md-4 mb-0'),
                Column('status', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('life_style', css_class='form-group col-md-6 mb-0'),
                Column('health_problem', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )



class BloodBankForm(ModelForm):
    collection_date = DateField(required=True, widget=DateInput)
    expiry_date = DateField(required=True, widget=DateInput)
    class Meta:
        model = BloodBank
        fields = ('donor','collection_date','expiry_date')

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request").user
        super(BloodBankForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'bloodbank_form'
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column('donor', css_class='form-group col-md-6 mb-0'),
            ),
            Row(
                Column('collection_date', css_class='form-group col-md-6 mb-0'),
                Column('expiry_date', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )


class DonorForm(ModelForm):
    dob = DateField(required=True, widget=DateInput)
    class Meta:
        model = Donor
        fields = ('name','phone','address','dob','bloodtype','status')

    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop("request").user
        super(DonorForm,self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_id = 'donor_form'
        # self.helper.add_input(Submit('submit', 'Save person'))

        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                Column(PrependedText('phone', '', placeholder="Phone"),
                       css_class='form-group col-md-6 mb-0'),
            ),
            Row(
                Column(PrependedText('bloodtype', '', placeholder="Blood Type"),
                       css_class='form-group col-md-6 mb-0'),
                Column('dob', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column(PrependedText('address', '', placeholder="Address"),
                       css_class='form-group col-md-6 mb-0'),
                Column('status', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),

            Submit('submit', 'Save', css_class='btn btn-block submit_button custom-placeholder'),
            Button('button', 'Close', css_class='btn- btn-label-warning submit_button waves-effect',
                   css_id='close_modal')
        )