import uuid

from django.contrib.auth.models import User, Group
from django.db import models
from datetime import date,timedelta
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime

from apps.helpers.models import TimeStamp, OperatorStamp
from apps.helpers.utils import CAMPAIGN_TYPE, STATUS_TYPE, INVOICE_STATUS, VISIT_TYPE, LIFE_STYLE, HEALTH_PPROBLEM, \
    ANC_TYPE, DELIVERY_CENTER, PregnancyCycle_TYPE, DELIVERY_TYPE, BIRTH_TYPE


class CompanyType(TimeStamp, OperatorStamp):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Company(TimeStamp, OperatorStamp):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='institute', null=True, blank=True)
    type = models.ForeignKey(CompanyType, null=True, blank=True, on_delete=models.SET_NULL)
    code = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    status = models.IntegerField(default=0, choices=STATUS_TYPE)
    cpm = models.DecimalField(max_digits=19, decimal_places=4, null=True, blank=True)

    def __str__(self):
        return str(self.name)


@receiver(post_save, sender=Company)
def update_code(sender, instance, **kwargs):
    if not instance.code:
        instance.code = 'C-' + str(1000 + instance.pk)
        post_save.disconnect(update_code, sender=Company)
        instance.save()
        post_save.connect(update_code, sender=Company)


class GroupProfile(TimeStamp, OperatorStamp):
    name = models.OneToOneField(Group, on_delete=models.CASCADE, null=True, blank=True)
    can_view_dashboard = models.BooleanField(default=False)
    # can_see_all_application = models.BooleanField(default=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    campaigns = models.JSONField(default=list,null=True,blank=True)

    def __str__(self):
        return self.name.name


#
# @receiver(post_save, sender=Group)
# def create_group_profile(sender, instance, **kwargs):
#     try:
#         GroupProfile.objects.create(name=instance)
#         # post_save.disconnect(update_code, sender=Group)
#         # instance.save()
#         # post_save.connect(update_code, sender=Group)
#     except:
#         pass
class Zilla(TimeStamp, OperatorStamp):
    zilla = models.CharField(max_length=255)

    def __str__(self):
        return str(self.zilla)

class Upazila(TimeStamp, OperatorStamp):
    upazila = models.CharField(max_length=255)
    zilla = models.ForeignKey(Zilla, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return str(self.upazila)

class Union(TimeStamp, OperatorStamp):
    union = models.CharField(max_length=255)
    upazila = models.ForeignKey(Upazila, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return str(self.union)

class Ward(TimeStamp, OperatorStamp):
    ward = models.CharField(max_length=255)
    union = models.ForeignKey(Union, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return str(self.ward)

class Profile(TimeStamp, OperatorStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    avatar = models.ImageField(null=True, blank=True)
    mobile = models.CharField(max_length=15, null=True, blank=True)
    ward = models.ForeignKey(Ward, on_delete=models.SET_NULL, null=True, blank=True)
    union = models.ForeignKey(Union, on_delete=models.SET_NULL, null=True, blank=True)
    upazila = models.ForeignKey(Upazila, on_delete=models.SET_NULL, null=True, blank=True)
    zilla = models.ForeignKey(Zilla, on_delete=models.SET_NULL, null=True, blank=True)
    # company = models.CharField(max_length=255, null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True, default='')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, **kwargs):
    try:
        Profile.objects.create(user=instance)
        # post_save.disconnect(update_code, sender=Group)
        # instance.save()
        # post_save.connect(update_code, sender=Group)
    except:
        pass


INVENTORY_TYPE = (
    (1, 'Website'),
    (2, 'Android App'),
    (3, 'iOS App'),
)


class Staff(TimeStamp, OperatorStamp):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    status = models.IntegerField(default=0, choices=STATUS_TYPE)
    joining_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return str(self.first_name)



class Patient(TimeStamp, OperatorStamp):
    agency = models.ForeignKey('accounts.Company', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    registration_no = models.CharField(max_length=255, null=True, blank=True)
    patient_name = models.CharField(max_length=255, null=True, blank=True)
    husband_name = models.CharField(max_length=255, null=True, blank=True)
    patient_phone = models.CharField(max_length=20, null=True, blank=True)
    husband_phone = models.CharField(max_length=20, null=True, blank=True)
    couple_number = models.IntegerField(default=0, null=True, blank=True)
    nid_number = models.IntegerField(default=0, null=True, blank=True)
    road = models.TextField(null=True, blank=True)
    ward = models.ForeignKey(Ward, on_delete=models.SET_NULL, null=True, blank=True)
    union = models.ForeignKey(Union, on_delete=models.SET_NULL, null=True, blank=True)
    upazila = models.ForeignKey(Upazila, on_delete=models.SET_NULL, null=True, blank=True)
    zilla = models.ForeignKey(Zilla, on_delete=models.SET_NULL, null=True, blank=True)
    patient_age = models.DateField(null=True, blank=True)
    husband_age = models.DateField(null=True, blank=True)
    patient_blood_group = models.CharField(max_length=20, null=True, blank=True)
    husband_blood_group = models.CharField(max_length=20, null=True, blank=True)
    status = models.IntegerField(default=0, choices=STATUS_TYPE)

    def __str__(self):
        return str(self.patient_name)

    # def save(self, *args, **kwargs):
    #     # Check if patient_age is less than 18 or more than 35
    #     if self.patient_age:
    #         age = (date.today() - self.patient_age) // timedelta(days=365)
    #         if age < 18 or age > 35:
    #             self.patient_status = 1
    #
    #     # Check if life_style or health_problem have values
    #     if self.life_style or self.health_problem:
    #         self.patient_status = 1
    #
    #     # Save patient_status based on conditions
    #     if not self.patient_status:
    #         self.patient_status = 0
    #
    #     creating = self.pk is None
    #     super().save(*args, **kwargs)
    #
    #     if creating and self.lmp:  # Check if lmp date is provided
    #         # Calculate appointment dates
    #         appointment_dates = [
    #             self.lmp + timedelta(days=120),  # One month plus
    #             self.lmp + timedelta(days=150),  # Two months plus
    #             self.lmp + timedelta(days=210)  # Three months plus
    #         ]
    #         anc_type_counter = 1
    #         # Create appointments
    #         for appointment_date in appointment_dates:
    #             Appointment.objects.create(
    #                 patient=self,
    #                 next_appointment_date=appointment_date,
    #                 agency=self.agency,
    #                 anc_type=anc_type_counter
    #
    #             )
    #             anc_type_counter += 1

class PregnancyCycle(TimeStamp, OperatorStamp):
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    agency = models.ForeignKey('accounts.Company', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    period_off_time = models.CharField(max_length=20, null=True, blank=True)
    pregnancy_number = models.IntegerField(max_length=20, null=True, blank=True)
    alive_children_number = models.IntegerField(max_length=20, null=True, blank=True)
    last_children_age = models.IntegerField(max_length=20, null=True, blank=True)
    normal_delivery_number = models.IntegerField(max_length=20, null=True, blank=True)
    csec_delivery_number = models.IntegerField(max_length=20, null=True, blank=True)
    mr_number = models.IntegerField(max_length=20, null=True, blank=True)
    monthly_income = models.IntegerField(max_length=20, null=True, blank=True)
    delivery_center = models.CharField(max_length=20, choices=DELIVERY_CENTER, null=True, blank=True)
    tt_dose_number = models.IntegerField(max_length=20, null=True, blank=True)
    after_delivery_plan = models.CharField(max_length=255, null=True, blank=True)
    assist_in_childbirth = models.CharField(max_length=255, null=True, blank=True)
    complications = models.CharField(max_length=255, null=True, blank=True)
    lmp = models.DateField(null=True, blank=True)
    edd = models.DateField(null=True, blank=True)
    delivery_complete_date = models.DateField(null=True, blank=True)
    life_style = models.JSONField(default=list, null=True, blank=True)
    health_problem = models.JSONField(default=list, null=True, blank=True)
    patient_status = models.IntegerField(default=0, max_length=20, null=True, blank=True)
    delivery_type = models.IntegerField(default=0, choices=DELIVERY_TYPE)
    birth_type = models.IntegerField(default=0, choices=BIRTH_TYPE)
    status = models.IntegerField(default=0, choices=PregnancyCycle_TYPE)

    def __str__(self):
        return str(self.pregnancy_number)

    def save(self, *args, **kwargs):
        # Check if patient_age is less than 18 or more than 35
        if self.patient and self.patient.patient_age:
            age = (date.today() - self.patient.patient_age) // timedelta(days=365)
            if age < 18 or age > 35:
                self.patient_status = 1

        # Check if life_style or health_problem have values
        if self.life_style or self.health_problem:
            self.patient_status = 1

        # Save patient_status based on conditions
        if not self.patient_status:
            self.patient_status = 0

        creating = self.pk is None
        super().save(*args, **kwargs)

        if creating and self.lmp:  # Check if lmp date is provided
            # Calculate appointment dates
            appointment_dates = [
                self.lmp + timedelta(days=120),  # One month plus
                self.lmp + timedelta(days=150),  # Two months plus
                self.lmp + timedelta(days=210)  # Three months plus
            ]
            anc_type_counter = 1
            # Create appointments
            for appointment_date in appointment_dates:
                Appointment.objects.create(
                    pregnancycycle=self,
                    patient=self.patient,
                    next_appointment_date=appointment_date,
                    agency=self.patient.agency,
                    anc_type=anc_type_counter

                )
                anc_type_counter += 1


class LifeStyle(TimeStamp, OperatorStamp):
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return str(self.name)

class HealthProblem(TimeStamp, OperatorStamp):
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Appointment(TimeStamp, OperatorStamp):
    agency = models.ForeignKey('accounts.Company', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True, blank=True)
    pregnancycycle = models.ForeignKey(PregnancyCycle, on_delete=models.SET_NULL, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    prescription = models.TextField(null=True, blank=True)
    next_appointment_date = models.DateField(null=True, blank=True)
    visited_date = models.DateField(null=True, blank=True)
    bp = models.CharField(max_length=20, null=True, blank=True)
    pulse = models.CharField(max_length=20, null=True, blank=True)
    rbs = models.CharField(max_length=20, null=True, blank=True)
    fundal_height = models.CharField(max_length=20, null=True, blank=True)
    placema_location = models.CharField(max_length=20, null=True, blank=True)
    iud = models.CharField(max_length=20, null=True, blank=True)
    preeclampsia = models.CharField(max_length=20, null=True, blank=True)
    anc_type = models.IntegerField(default=0,choices=ANC_TYPE)
    status = models.IntegerField(default=0, choices=VISIT_TYPE)

    def __str__(self):
        return str(self.next_appointment_date)

class Donor(TimeStamp, OperatorStamp):
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=20, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    bloodtype = models.CharField(max_length=20, null=True, blank=True)
    status = models.IntegerField(default=0, choices=STATUS_TYPE)

    def __str__(self):
        return str(self.name)


class BloodBank(TimeStamp, OperatorStamp):
    donor = models.ForeignKey(Donor, on_delete=models.SET_NULL, null=True, blank=True)
    collection_date = models.DateField(null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return str(self.collection_date)

class StaffWorkingLog(TimeStamp, OperatorStamp):
    agency = models.ForeignKey('accounts.Company', related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.patient)