from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.accounts.models import Company, Profile, Staff, Patient, Appointment, BloodBank, Donor, PregnancyCycle, \
    StaffWorkingLog, Upazila, Union, Ward

import re


class CompanySerializer(serializers.ModelSerializer):
    type_name = serializers.SerializerMethodField(read_only=True)

    def get_type_name(self, obj):
        try:
            return obj.type.name
        except:
            return ''

    class Meta:
        model = Company
        fields = '__all__'

    def validate_type(self, value):
        if not value:
            raise ValidationError("This field may not be blank.")
        return value

class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = '__all__'

class PatientSerializer(serializers.ModelSerializer):
    agency_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='agency.name', read_only=True)
    user_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='user.first_name', read_only=True)

    class Meta:
        model = Patient
        fields = '__all__'

class BloodBankSerializer(serializers.ModelSerializer):
    name = serializers.PrimaryKeyRelatedField(allow_null=True, source='donor.name', read_only=True)
    bloodtype = serializers.PrimaryKeyRelatedField(allow_null=True, source='donor.bloodtype', read_only=True)
    class Meta:
        model = BloodBank
        fields = '__all__'

class DonorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Donor
        fields = '__all__'

class PregnancyCycleSerializer(serializers.ModelSerializer):
    agency_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='agency.name', read_only=True)
    patient_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='patient.patient_name', read_only=True)
    delivery_complete_date = serializers.DateField(format="%Y-%m-%d", allow_null=True, read_only=True)
    class Meta:
        model = PregnancyCycle
        fields = '__all__'

class StaffWorkingLogSerializer(serializers.ModelSerializer):
    agency_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='agency.name', read_only=True)
    user_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='user.first_name', read_only=True)
    patient_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='patient.patient_name', read_only=True)
    class Meta:
        model = StaffWorkingLog
        fields = '__all__'


class UpazilaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Upazila
        fields = '__all__'

class UnionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Union
        fields = '__all__'

class WardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ward
        fields = '__all__'

class AppointmentSerializer(serializers.ModelSerializer):
    agency_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='agency.name', read_only=True)
    patient_name = serializers.PrimaryKeyRelatedField(allow_null=True, source='patient.patient_name', read_only=True)
    class Meta:
        model = Appointment
        fields = '__all__'

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')


class UserSerializer(serializers.ModelSerializer):
    # groups = GroupSerializer(many=True,required=False)
    username = serializers.EmailField(required=True,label='Email')
    first_name = serializers.CharField(required=True, label='Full Name')
    name = serializers.CharField(source='get_full_name', read_only=True)
    password = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    group = serializers.SerializerMethodField(read_only=True)
    last_login = serializers.DateTimeField(format="%b %d %Y | %I:%M %p", required=False, read_only=True)

    # mobile = serializers.CharField(required=True,source='profile.mobile')
    company = serializers.PrimaryKeyRelatedField(source='profile.company.name', read_only=True, allow_null=True)

    def get_group(self, obj):
        return GroupSerializer(obj.groups, many=True).data

    class Meta:
        model = User
        # fields = ('id','username','email','groups','is_staff','is_active','is_superuser','name')
        fields = '__all__'

    def validate_groups(self, data):
        print(data)
        if not data:
            raise ValidationError("This field may not be blank.")
        return data

    def validate_company(self, data):
        print(data)
        if not data:
            raise ValidationError("This field may not be blank.")
        return data

    def validate_mobile(self, data):
        print(data)
        if not data:
            raise ValidationError("This field may not be blank.")
        return data

    # def validate_username(self, data):
    #     regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    #     if not (re.fullmatch(regex, data)):
    #         raise ValidationError("Enter a valid email address.")

    # def validate_password2(self, data):
    #     if not data:
    #         raise ValidationError("This field may not be blank.")

    def validate(self, data):
        password1 = data.get('password', None)
        password2 = data.get('password2', None)
        if password1 != password2:
            raise serializers.ValidationError({
                "password": ["Password not match with confirm password"],
            })
        return super(UserSerializer, self).validate(data)


    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            first_name=validated_data['first_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        for item in validated_data['groups']:
            user.groups.add(item)
        return user


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'
