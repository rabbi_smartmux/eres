from django.urls import path

from . import views
from apps.accounts.views import LogoutView, CustomLoginView, RolesView, RoleCreateView, UserViewSet, UserListView, \
    UserCreateView, UserEditView, CompanyViewSet, ComapnyListView, CompanyEditView, PermissionView, RoleEditView, \
    CompanyCreateView, CompanyListByTypeView, CompanyCreateByTypeView, \
    InvoiceListView, StaffListView, StaffViewSet, StaffCreateView, StaffEditView, StaffDetailView, PatientViewSet, \
    PatientListView, PatientCreateView, PatientEditView, PatientDetailView, AppointmentListView, AppointmentCreateView, \
    AppointmentEditView, AppointmentDetailView, AppointmentViewSet, PatientDetailAdminView, UpcomingAppointmentListView, \
    HistoricalAppointmentListView, NotvisitedAppointmentListView, BloodBankListView, BloodBankCreateView, \
    BloodBankEditView, BloodBankDetailView, BloodBankViewSet, DonorListView, DonorCreateView, DonorViewSet, \
    DonorEditView, DonorDetailView, PregnancyCycleListView, PregnancyCycleCreateView, PregnancyCycleEditView, \
    PregnancyCycleDetailView, PregnancyCycleViewSet, StaffWorkingLogListView, StaffWorkingLogCreateView, \
    StaffWorkingLogEditView, StaffWorkingLogDetailView, StaffWorkingLogViewSet, UpazilaViewSet, UnionViewSet, \
    WardViewSet
from dsp.urls import router

router.register(r'users', UserViewSet, basename='user_api')
router.register(r'company', CompanyViewSet, basename='company_api')
router.register(r'staff', StaffViewSet, basename='staff_api')
router.register(r'patient', PatientViewSet, basename='patient_api')
router.register(r'appointment', AppointmentViewSet, basename='appointment_api')
router.register(r'bloodbank', BloodBankViewSet, basename='bloodbank_api')
router.register(r'donor', DonorViewSet, basename='donor_api')
router.register(r'pregnancycycle', PregnancyCycleViewSet, basename='pregnancycycle_api')
router.register(r'staffworkinglog', StaffWorkingLogViewSet, basename='staffworkinglog_api')
router.register(r'upazila', UpazilaViewSet, basename='upazila_api')
router.register(r'union', UnionViewSet, basename='union_api')
router.register(r'ward', WardViewSet, basename='ward_api')

urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('roles/', RolesView.as_view(), name='roles'),
    path('roles/create', RoleCreateView.as_view(), name='roles_create'),
    path('roles/<int:pk>/edit', RoleEditView.as_view(), name='roles_edit'),

    path('users/', UserListView.as_view(), name='users'),
    path('user/create', UserCreateView.as_view(), name='user_create'),
    path('user/<int:pk>/edit', UserEditView.as_view(), name='user_edit'),

    path('company/', ComapnyListView.as_view(), name='company'),
    path('staff/', StaffListView.as_view(), name='staff'),
    path('staff/create', StaffCreateView.as_view(), name='staff_create'),
    path('staff/<int:pk>/edit', StaffEditView.as_view(), name='staff_edit'),
    path('staff/<int:pk>/detail', StaffDetailView.as_view(), name='staff_detail'),

    path('patient/', PatientListView.as_view(), name='patient'),
    path('patient/create', PatientCreateView.as_view(), name='patient_create'),
    path('patient/<int:pk>/edit', PatientEditView.as_view(), name='patient_edit'),
    path('patient/<int:pk>/detail', PatientDetailView.as_view(), name='patient_detail'),
    path('patient/<int:pk>/detail_patient/', PatientDetailAdminView.as_view(), name='patient_admin_detail'),

    path('patient_appoinment_api', views.PatientAppoinmentAPI.as_view(), name='patient_appoinment_api'),
    path('appointment/', AppointmentListView.as_view(), name='appointment'),
    path('upcomming_appointment/', UpcomingAppointmentListView.as_view(), name='upcomming_appointment'),
    path('historical_appointment/', HistoricalAppointmentListView.as_view(), name='historical_appointment'),
    path('notvisited_appointment/', NotvisitedAppointmentListView.as_view(), name='notvisited_appointment'),
    path('appointment/create', AppointmentCreateView.as_view(), name='appointment_create'),
    path('appointment/<int:pk>/edit', AppointmentEditView.as_view(), name='appointment_edit'),
    path('appointment/<int:pk>/detail', AppointmentDetailView.as_view(), name='appointment_detail'),

    path('company/type/<str:type>', CompanyListByTypeView.as_view(), name='company_by_type'),
    path('company/create', CompanyCreateView.as_view(), name='company_create'),

    path('bloodbank/', BloodBankListView.as_view(), name='bloodbank'),
    path('bloodbank/create', BloodBankCreateView.as_view(), name='bloodbank_create'),
    path('bloodbank/<int:pk>/edit', BloodBankEditView.as_view(), name='bloodbank_edit'),
    path('bloodbank/<int:pk>/detail', BloodBankDetailView.as_view(), name='bloodbank_detail'),


    path('pregnancycycle/', PregnancyCycleListView.as_view(), name='pregnancycycle'),
    path('pregnancycycle/create', PregnancyCycleCreateView.as_view(), name='pregnancycycle_create'),
    path('pregnancycycle/<int:pk>/edit', PregnancyCycleEditView.as_view(), name='pregnancycycle_edit'),
    path('pregnancycycle/<int:pk>/detail', PregnancyCycleDetailView.as_view(), name='pregnancycycle_detail'),

    path('donor/', DonorListView.as_view(), name='donor'),
    path('donor/create', DonorCreateView.as_view(), name='donor_create'),
    path('donor/<int:pk>/edit', DonorEditView.as_view(), name='donor_edit'),
    path('donor/<int:pk>/detail', DonorDetailView.as_view(), name='donor_detail'),

    path('staffworkinglog/', StaffWorkingLogListView.as_view(), name='staffworkinglog'),
    path('staffworkinglog/create', StaffWorkingLogCreateView.as_view(), name='staffworkinglog_create'),
    path('staffworkinglog/<int:pk>/edit', StaffWorkingLogEditView.as_view(), name='staffworkinglog_edit'),
    path('staffworkinglog/<int:pk>/detail', StaffWorkingLogDetailView.as_view(), name='staffworkinglog_detail'),


    path('company/<int:type>/create', CompanyCreateByTypeView.as_view(), name='company_create'),
    path('company/<int:pk>/edit', CompanyEditView.as_view(), name='company_edit'),


    path('permission', PermissionView.as_view(), name='permission_list'),

]
