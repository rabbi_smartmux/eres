import base64
import datetime

from django.contrib.auth import logout, login
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth.views import LoginView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Q, Count, Sum, Max, Min
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import View
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DetailView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status, pagination
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView
from rest_framework_datatables.filters import DatatablesFilterBackend

from apps.accounts.forms import CustomAuthenticationForm, GroupForm, UserForm, CompanyForm, GroupMultiForm, \
    GroupEditMultiForm, CompanyFormByType, StaffForm, PatientForm, AppointmentForm, BloodBankForm, DonorForm, \
    PregnancyCycleForm, StaffWorkingLogForm
from apps.accounts.models import Company, CompanyType, Profile, Staff, Patient, Appointment, BloodBank, Donor, \
    PregnancyCycle, StaffWorkingLog, Upazila, Union, Ward, Zilla
from apps.accounts.serializers import UserSerializer, CompanySerializer, UserProfileSerializer, StaffSerializer, \
    PatientSerializer, AppointmentSerializer, BloodBankSerializer, DonorSerializer, PregnancyCycleSerializer, \
    StaffWorkingLogSerializer, UpazilaSerializer, WardSerializer, UnionSerializer
from apps.accounts.utils import PassRequestToFormViewMixin
from apps.helpers.helper import template


class CustomLoginView(LoginView):
    form_class = CustomAuthenticationForm
    template_name = template('auth/login.html')

    def render_to_response(self, context, **response_kwargs):

        response = super(CustomLoginView, self).render_to_response(context, **response_kwargs)
        cookies = self.request.COOKIES

        if self.request.user.is_authenticated:
            return HttpResponseRedirect('/')
        return response

    def form_valid(self, form):
        username = form.cleaned_data['username']
        user = User.objects.get(username=username)

        # TODO::Redurect auth user by there permission
        # if not user.is_staff:
        #     raise PermissionDenied

        login(self.request, form.get_user())

        response = HttpResponseRedirect(self.get_success_url())
        username = self.request.user.username
        encoded = base64.b64encode(username.encode())
        response.set_cookie('un', encoded.decode(), 7200)  # 2 Hours
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['domain'] = Institute.objects.get(sdomain=self.request.sdomain.sdomain)
        return context


class LogoutView(View):

    def get(self, request):
        response = logout(request)
        response = HttpResponseRedirect('/accounts/login/')
        response.delete_cookie('un')
        return response


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    filter_backends = (DjangoFilterBackend,DatatablesFilterBackend)

    filterset_fields = ['type__name','code']


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class StaffViewSet(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer
    filter_backends = (DjangoFilterBackend,DatatablesFilterBackend)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    filter_backends = (DjangoFilterBackend,DatatablesFilterBackend)

    def get_queryset(self):
        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            self.queryset = self.queryset.filter(agency=self.request.user.profile.company)
        return self.queryset

    def create(self, request, *args, **kwargs):

        data = request.data.copy()

        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            data['agency'] = self.request.user.profile.company.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class BloodBankViewSet(viewsets.ModelViewSet):
    queryset = BloodBank.objects.all()
    serializer_class = BloodBankSerializer
    filter_backends = (DjangoFilterBackend, DatatablesFilterBackend)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class DonorViewSet(viewsets.ModelViewSet):
    queryset = Donor.objects.all()
    serializer_class = DonorSerializer
    filter_backends = (DjangoFilterBackend, DatatablesFilterBackend)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class PregnancyCycleViewSet(viewsets.ModelViewSet):
    queryset = PregnancyCycle.objects.all()
    serializer_class = PregnancyCycleSerializer
    filter_backends = (DjangoFilterBackend, DatatablesFilterBackend)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class StaffWorkingLogViewSet(viewsets.ModelViewSet):
    queryset = StaffWorkingLog.objects.all()
    serializer_class = StaffWorkingLogSerializer
    filter_backends = (DjangoFilterBackend, DatatablesFilterBackend)

    def get_queryset(self):
        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            self.queryset = self.queryset.filter(user=self.request.user)
        return self.queryset

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, agency=self.request.user.profile.company)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user, agency=self.request.user.profile.company)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class UpazilaViewSet(viewsets.ModelViewSet):
    queryset = Upazila.objects.all().order_by('-id')
    serializer_class = UpazilaSerializer
    # pagination_class = True
    pagination.PageNumberPagination.page_size = 50
    # advertiseradgroupsbanners__adgroup__campaign = 1909
    filterset_fields = ['zilla']
    # permission_classes = ()


class UnionViewSet(viewsets.ModelViewSet):
    queryset = Union.objects.all().order_by('-id')
    serializer_class = UnionSerializer
    # pagination_class = True
    pagination.PageNumberPagination.page_size = 50
    # advertiseradgroupsbanners__adgroup__campaign = 1909
    filterset_fields = ['upazila']

class WardViewSet(viewsets.ModelViewSet):
    queryset = Ward.objects.all().order_by('-id')
    serializer_class = WardSerializer
    # pagination_class = True
    pagination.PageNumberPagination.page_size = 50
    # advertiseradgroupsbanners__adgroup__campaign = 1909
    filterset_fields = ['union']

class PatientListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_patient'
    template_name = template('company/app-patient-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return context

class BloodBankListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_bloodbank'
    template_name = template('company/app-bloodbank-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context


class PregnancyCycleListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_pregnancycycle'
    template_name = template('company/app-pregnancycycle-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context


class DonorListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_donor'
    template_name = template('company/app-donor-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context

class StaffWorkingLogListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_staffworkinglog'
    template_name = template('company/app-staffworkinglog-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context

class StaffWorkingLogCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_staffworkinglog'
    template_name = template('company/app-staffworkinglog-add.html')

    form_class = StaffWorkingLogForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.agency = self.request.user.profile.company
        return super().form_valid(form)

class StaffWorkingLogEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_patient'
    template_name = template('company/app-patient-edit.html')

    form_class = PatientForm
    context_object_name = 'patient'
    queryset = Patient.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class StaffWorkingLogDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-donor-details.html')
    queryset = Donor.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class StaffListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_company'
    template_name = template('company/app-staff-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context

class StaffCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_company'
    template_name = template('company/app-staff-add.html')

    form_class = StaffForm
    success_url = reverse_lazy('staff')
    success_message = "Requested staff added successfully"

    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(UserCreateView, self).form_valid(form)

class PatientCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_patient'
    template_name = template('company/app-patient-add.html')

    form_class = PatientForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        print(kwargs)
        return kwargs

class BloodBankCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_bloodbank'
    template_name = template('company/app-bloodbank-add.html')

    form_class = BloodBankForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

class PregnancyCycleCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_pregnancycycle'
    template_name = template('company/app-pregnancycycle-add.html')

    form_class = PregnancyCycleForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class DonorCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_donor'
    template_name = template('company/app-donor-add.html')

    form_class = DonorForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

class StaffEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_company'
    template_name = template('company/app-staff-edit.html')

    form_class = StaffForm
    success_url = reverse_lazy('staff')
    success_message = "Requested staff updated successfully"
    context_object_name = 'staff'
    queryset = Staff.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(StaffEditView, self).form_valid(form)
# class AppointmentEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
#     permission_required = 'accounts.change_appointment'
#     template_name = template('company/app-appointment-edit.html')
#
#     form_class = AppointmentForm
#     context_object_name = 'appointment'
#     queryset = Appointment.objects.all()
#
#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs.update({'request': self.request})
#         return kwargs
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#
#         return context
class PatientEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_patient'
    template_name = template('company/app-patient-edit.html')

    form_class = PatientForm
    context_object_name = 'patient'
    queryset = Patient.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(PatientEditView, self).form_valid(form)


class BloodBankEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_bloodbank'
    template_name = template('company/app-bloodbank-edit.html')

    form_class = BloodBankForm
    context_object_name = 'bloodbank'
    queryset = BloodBank.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class DonorEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_donor'
    template_name = template('company/app-donor-edit.html')

    form_class = DonorForm
    context_object_name = 'donor'
    queryset = Donor.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(DonorEditView, self).form_valid(form)


class StaffDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'campaigns.change_campaign'
    template_name = template('company/app-staff-details.html')
    queryset = Staff.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class PatientDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-patient-details.html')
    queryset = Patient.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class BloodBankDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-bloodbank-details.html')
    queryset = BloodBank.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class PregnancyCycleDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-pregnancycycle-details.html')
    queryset = PregnancyCycle.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class DonorDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-donor-details.html')
    queryset = Donor.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class PatientDetailAdminView(LoginRequiredMixin, DetailView):
    queryset = Patient.objects.all()
    template_name = template('company/detail.html')
    # slug_field = 'code'

    def get_context_data(self, **kwargs):
        # campaign_code = self.kwargs.get('code')
        # today = datetime.datetime.today()
        # seven_day_ago = datetime.datetime.today() - datetime.timedelta(days=6)
        #
        context = super().get_context_data(**kwargs)
        context['object'] = Appointment.objects.filter(patient=self.object).first()
        # context['my_list'] = [50,70,75,67,63]
        #
        # context['daterange'] = seven_day_ago.strftime('%m/%d/%Y') + ' - ' + today.strftime('%m/%d/%Y')
        return context

class PatientAppoinmentAPI(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        cm_id = self.request.query_params.get('id')

        campaign_summery = Appointment.objects.filter(patient=cm_id).values('anc_type','preeclampsia','iud','placema_location','fundal_height','rbs','pulse','bp','visited_date','next_appointment_date','prescription','comment','status')
        print(campaign_summery)

        # print(campaign_summery)
        return Response(campaign_summery)


class UserViewSet(viewsets.ModelViewSet):
    # http_method_names = ['get', 'post']
    queryset = User.objects.all().exclude(is_superuser=1)
    serializer_class = UserSerializer
    filter_backends = (DjangoFilterBackend,DatatablesFilterBackend)

    def get_queryset(self):
        print(self.request.user.is_staff)
        if not self.request.user.is_staff:
            self.queryset = self.queryset.filter(profile__company=self.request.user.profile.company)
        return self.queryset

    def create(self, request, *args, **kwargs):
        with transaction.atomic():
            data = request.data.copy()

            password1 = data.get('password1')
            password2 = data.get('password2')

            data['username'] = data.get('email')
            data['password'] = password1

            try:
                group = Group.objects.get(id=data.get('group'))
                if group:
                    data['groups'] = [group.id]
            except:
                data['groups']=None


            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            if password1 != password2:
                return Response()

            self.perform_create(serializer)

            serialize_data = serializer.data
            print(data.get('id'),data,serialize_data)
            profile = Profile.objects.get(user_id=serialize_data.get('id'))
            # print(profile,data.get('mobile'))
            profile.mobile = data.get('mobile')
            profile.company_id = data.get('company')
            profile.location = data.get('location')
            profile.zilla_id = data.get('zilla')
            profile.upazila_id = data.get('upazila')
            profile.union_id = data.get('union')
            profile.ward_id = data.get('ward')
            profile.save()

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        data = request.data.copy()
        if data.get('email'):
            data['username'] = data.get('email')

        print(data)

        try:
            group = Group.objects.get(id=data.get('group'))
            if group:
                data['groups'] = [group.id]
        except:
            data['groups'] = None

        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        profile = Profile.objects.get(user=instance)
        print(profile)

        profile_serializer = UserProfileSerializer(instance=profile,data=request.data,partial=True)
        profile_serializer.is_valid(raise_exception=True)
        profile_serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'auth.view_user'
    template_name = template('users/app-user-list.html')

    def get_context_data(self, **kwargs):
        context = {}
        dt = datetime.date.today()
        today = datetime.datetime.combine(dt, datetime.datetime.min.time())
        # yestarday = datetime.datetime.combine(dt, datetime.datetime.min.time())-datetime.timedelta(days=1)
        groups = Group.objects.all()
        company = Company.objects.filter(status=True)
        users = User.objects.all()
        if not self.request.user.is_staff:
            groups = groups.filter(groupprofile__company=self.request.user.profile.company)
            company = company.filter(id=self.request.user.profile.company.id)
            users = users.filter(profile__company=self.request.user.profile.company)
        context['groups'] = groups
        context['company'] = company

        context['active_user'] = users.filter(is_active=True).count()
        context['staff_user'] = users.filter(is_active=True,is_staff=True).count()
        context['pending_user'] = users.filter(is_active=False).count()
        context['logged_in_today'] = users.filter(last_login__gte=today).count()
        return  context


class UserCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'auth.add_user'
    template_name = template('users/app-user-add.html')

    form_class = UserForm
    success_url = reverse_lazy('users')
    success_message = "Requested user added successfully"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        groups = Group.objects.all()
        company = Company.objects.filter(status=True)
        zilla = Zilla.objects.all()
        upazila = Upazila.objects.all()
        union = Union.objects.all()
        ward = Ward.objects.all()
        if not self.request.user.is_staff:
            groups = groups.filter(groupprofile__company=self.request.user.profile.company)
            company = company.filter(id=self.request.user.profile.company.id)
        context['groups'] = groups
        context['company'] = company
        context['zilla'] = zilla
        context['upazila'] = upazila
        context['union'] = union
        context['ward'] = ward

        return context

    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(UserCreateView, self).form_valid(form)


class UserEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'auth.change_user'
    template_name = template('users/app-user-edit.html')

    form_class = UserForm
    success_url = reverse_lazy('users')
    success_message = "Requested user updated successfully"
    context_object_name = 'user'
    queryset = User.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        groups = Group.objects.all()
        company = Company.objects.filter(status=True)
        if not self.request.user.is_staff:
            groups = groups.filter(groupprofile__company=self.request.user.profile.company)
            company = company.filter(id=self.request.user.profile.company.id)
        context['groups'] = groups
        context['usrg']=self.object.groups.first()
        context['company'] = company

        return context


    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(UserCreateView, self).form_valid(form)




class ComapnyListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_company'
    template_name = template('company/app-company-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        context['company_type'] = CompanyType.objects.all()
        return  context

class InvoiceListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_invoice'
    template_name = template('invoice/app-invoice-list.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = {}
        return  context


class CompanyListByTypeView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_company'
    template_name = template('company/app-company-list-by-type.html')

    def get_context_data(self, **kwargs):
        print(self.kwargs)
        context = super().get_context_data(**kwargs)
        context['type_obj'] = CompanyType.objects.filter(name=self.kwargs.get('type')).first()
        context['company_type'] = CompanyType.objects.all()
        print(context)
        return  context


class CompanyCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_company'
    template_name = template('company/app-company-add.html')

    form_class = CompanyForm
    success_url = reverse_lazy('company')
    success_message = "Requested company added successfully"

    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(UserCreateView, self).form_valid(form)


class CompanyCreateByTypeView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_company'
    template_name = template('company/app-company-add.html')

    form_class = CompanyFormByType
    success_url = reverse_lazy('company')
    success_message = "Requested company added successfully"

    def get_form_kwargs(self):
        kwargs = super(CompanyCreateByTypeView, self).get_form_kwargs()

        kwargs.update(initial={'type':self.kwargs.get('type')})
        print(kwargs)
        return kwargs

    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(CompanyCreateByTypeView, self).form_valid(form)


class CompanyEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_company'
    template_name = template('company/app-company-edit.html')

    form_class = CompanyForm
    success_url = reverse_lazy('company')
    success_message = "Requested company updated successfully"
    context_object_name = 'company'
    queryset = Company.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['company_type'] = CompanyType.objects.all()

        return context


    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        form.save()

        return super(CompanyEditView, self).form_valid(form)


class RolesView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = template('roles/app-access-roles.html')
    permission_required = 'auth.view_group'
    model = Group
    context_object_name = 'groups'

    def get_queryset(self):
        group_objects = Group.objects.all().prefetch_related('user_set')
        if not self.request.user.is_staff:
            group_objects = group_objects.filter(groupprofile__company=self.request.user.profile.company)
        queryset = group_objects

        return queryset


class RoleCreateView(LoginRequiredMixin, PermissionRequiredMixin, PassRequestToFormViewMixin, SuccessMessageMixin,
                     CreateView):
    permission_required = 'auth.add_group'
    form_class = GroupMultiForm
    template_name = template('roles/access-roles-add.html')
    success_url = reverse_lazy('roles')
    success_message = "Requested team added successfully"

    def form_valid(self, form):
        # Save the user first, because the profile needs a user before it
        # can be saved.
        group = form['group'].save()
        profile = form['profile'].save(commit=False)
        profile.name = group
        # institute = self.request.user.userprofile.institute
        profile.created_by = self.request.user
        profile.updated_by = self.request.user
        # profile.institute = institute
        profile.save()
        return super(RoleCreateView, self).form_valid(form)







class RoleEditView(LoginRequiredMixin, PermissionRequiredMixin, PassRequestToFormViewMixin, SuccessMessageMixin,
                     UpdateView):
    permission_required = 'auth.change_group'
    form_class = GroupEditMultiForm
    template_name = template('roles/access-roles-add.html')
    success_url = reverse_lazy('roles')
    success_message = "Requested team updated successfully"
    queryset = Group.objects.all()

    def get_form_kwargs(self):
        kwargs = super(RoleEditView, self).get_form_kwargs()
        kwargs.update(instance={
            'group': self.object,
            'profile': self.object.groupprofile,
        })
        return kwargs


    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['company'] = Company.objects.filter(status=True)
    #     return context
    #
    # def form_valid(self, form):
    #     # Save the user first, because the profile needs a user before it
    #     # can be saved.
    #     print(form.cleaned_data['company'],'Form Clean Data')
    #     role = form.save()
    #     try:
    #         role.groupprofile.company=form.cleaned_data['company']
    #         role.groupprofile.save()
    #     except:
    #         pass
    #
    #     return super(RoleEditView, self).form_valid(form)


class PermissionView(TemplateView):
    template_name = template('permission/list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['permissions'] = Permission.objects.all()
        print(context)
        return context

class AppointmentListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_appointment'
    template_name = template('company/app-appointment-list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

class UpcomingAppointmentListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_appointment'
    template_name = template('company/upcomming-appointment-list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

class NotvisitedAppointmentListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_appointment'
    template_name = template('company/notvisited-appointment-list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

class HistoricalAppointmentListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = 'accounts.view_appointment'
    template_name = template('company/historical-appointment-list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

class AppointmentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'accounts.add_appointment'
    template_name = template('company/app-appointment-add.html')

    form_class = AppointmentForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs


class AppointmentEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_appointment'
    template_name = template('company/app-appointment-edit.html')

    form_class = AppointmentForm
    context_object_name = 'appointment'
    queryset = Appointment.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class PregnancyCycleEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'accounts.change_pregnancycycle'
    template_name = template('company/app-pregnancycycle-edit.html')

    form_class = PregnancyCycleForm
    context_object_name = 'pregnancycycle'
    queryset = PregnancyCycle.objects.all()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class AppointmentDetailView(LoginRequiredMixin, DetailView):
    template_name = template('company/app-appointment-details.html')
    queryset = Appointment.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['target'] = CampaignTargets.objects.get_or_create(campaign=self.object)
        return context

class AppointmentViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    filter_backends = (DjangoFilterBackend,DatatablesFilterBackend)

    def get_queryset(self):
        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            self.queryset = self.queryset.filter(agency=self.request.user.profile.company)

        return self.queryset

    @action(detail=False, methods=['get'])
    def status_1(self, request):
        queryset = self.get_queryset().filter(status=1)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def status_0(self, request):
        queryset = self.get_queryset().filter(status=0, next_appointment_date__gt=timezone.now())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    # @action(detail=False, methods=['get'])
    # def status_0_past(self, request):
    #     queryset = self.get_queryset().filter(status=0, next_appointment_date__lt=timezone.now())
    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def status_0(self, request):
        # Annotate the queryset with the latest next_appointment_date for each patient
        latest_appointments = self.get_queryset().filter(
            status=0,
            next_appointment_date__gt=timezone.now()
        ).values('patient').annotate(
            latest_date=Min('next_appointment_date')
        )

        # Filter the queryset to only include the latest appointment for each patient
        queryset = self.get_queryset().filter(
            status=0,
            next_appointment_date__gt=timezone.now(),
            next_appointment_date__in=[appointment['latest_date'] for appointment in latest_appointments]
        )

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):

        data = request.data.copy()

        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            data['agency'] = self.request.user.profile.company.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)