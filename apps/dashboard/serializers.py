from django.db.models import Count, Sum
from rest_framework import serializers
from django.utils import timezone
import datetime

from apps.accounts.models import Company, Appointment, Patient
from apps.helpers.views import date_start_time, date_end_time




class PatientSummerySerializer(serializers.ModelSerializer):
    request_response = serializers.SerializerMethodField(read_only=True)
    def get_request_response(self, obj):
        # search_data = self.context['search_customer']
        start_date = self.context.get('start_date')
        end_date = self.context.get('end_date')

        appointments = Appointment.objects.filter(patient=obj)
        total_appointments = appointments.count()
        today = datetime.datetime.today()
        upcomming_appointments = appointments.filter(next_appointment_date__lte=today).count()
        upcomming= total_appointments-upcomming_appointments
        return {"total_appointments":total_appointments,"upcomming_appointments":upcomming}

    class Meta:
        model = Patient
        fields = '__all__'
