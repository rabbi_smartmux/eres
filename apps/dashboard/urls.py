from django.urls import path

from apps.dashboard import views
from dsp.urls import router

urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard'),
    path('bloodcount', views.BloodCountView.as_view(), name='bloodcount'),
    path('api/v1/patient_data', views.PatientDashboardDataAPI.as_view(), name='patient_data'),
    path('api/v1/patient_report_data', views.PatientReportDashboardDataAPI.as_view(), name='patient_report_data'),
    path('api/v1/bloodcount/', views.BloodCountDataAPI.as_view(), name='bloodcount_data'),
]
