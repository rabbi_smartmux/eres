# from datetime import datetime, timedelta
from itertools import count
import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum, Count, Q
from django.shortcuts import render

# Create your views here.
from django.utils import timezone
from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.accounts.models import Company, Patient, BloodBank, Appointment, PregnancyCycle
from apps.accounts.serializers import PatientSerializer
from apps.dashboard.serializers import PatientSummerySerializer
from apps.helpers.helper import template
from apps.helpers.utils import ANC_TYPE, BIRTH_TYPE, DELIVERY_TYPE
from apps.helpers.views import date_start_time, date_end_time, int_or_zero


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = template('index.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = datetime.datetime.today()
        seven_day_ago = datetime.datetime.today() - datetime.timedelta(days=6)
        context['daterange'] = seven_day_ago.strftime('%m/%d/%Y')+' - '+ today.strftime('%m/%d/%Y')
        return context


    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated and not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            self.template_name = template('BrandDashboard.html')


        return super(DashboardView, self).dispatch(request, *args, **kwargs)


class BloodCountView(LoginRequiredMixin, TemplateView):
    template_name = template('company/app-bloodcount-list.html')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context



class PatientDashboardDataAPI(APIView):
    permission_classes = ()
    authentication_classes = (SessionAuthentication,)

    def get(self, request, *args, **kwargs):

        data = {}
        com = self.request.user.profile.company
        patient = Patient.objects.filter(status=1)
        if not self.request.user.is_superuser and self.request.user.profile.company.type_id == 2:
            company = self.request.user.profile.company
            patient = patient.filter(agency=company)
        # print(patient)
        # total_active_company = Company.objects.filter(type=2).filter(status=1).count()
        # total_company = Company.objects.filter(type=2).count()
        #
        serializer = PatientSummerySerializer(patient, many=True)
        data['patient_summery'] = serializer.data
        #
        # data['total_company'] = total_company
        # data['total_active_company'] = total_active_company
        # run_cmp = total_company - total_active_company
        # data['ended_company'] = run_cmp

        return Response(data)


# class PatientReportDashboardDataAPI(APIView):
#     permission_classes = ()
#     authentication_classes = (SessionAuthentication,)
#
#     def get(self, request, *args, **kwargs):
#         # Get current date and calculate the start of the last month
#         today = timezone.now().date()
#         first_day_of_current_month = today.replace(day=1)
#         last_day_of_last_month = first_day_of_current_month - datetime.timedelta(days=1)
#         first_day_of_last_month = last_day_of_last_month.replace(day=1)
#
#         # Calculate the end date of the current month
#         last_day_of_current_month = today.replace(day=1) + datetime.timedelta(days=31)
#         last_day_of_current_month = last_day_of_current_month.replace(day=1) - datetime.timedelta(days=1)
#
#         # Filter appointments in the last month
#         appointments_last_month = Appointment.objects.filter(
#             visited_date__range=[first_day_of_last_month, last_day_of_last_month],
#             status=1
#         )
#
#         # Filter appointments in the current month
#         appointments_current_month = Appointment.objects.filter(
#             visited_date__range=[first_day_of_current_month, today],
#             status=1
#         )
#
#         if not request.user.is_superuser and request.user.profile.company.type_id == 2:
#             company = request.user.profile.company
#             appointments_last_month = appointments_last_month.filter(agency=company)
#             appointments_current_month = appointments_current_month.filter(agency=company)
#
#         # Initialize counts for all ANC types to zero
#         anc_type_counts_last_month = {f"ANC_{anc_type[0]}": 0 for anc_type in ANC_TYPE}
#         anc_type_counts_current_month = {f"ANC_{anc_type[0]}": 0 for anc_type in ANC_TYPE}
#
#         # Count appointments by anc_type for last month
#         actual_counts_last_month = appointments_last_month.values('anc_type').annotate(count=Count('id'))
#         for item in actual_counts_last_month:
#             anc_type_key = f"ANC_{item['anc_type']}"
#             anc_type_counts_last_month[anc_type_key] = item['count']
#
#         # Count appointments by anc_type for current month
#         actual_counts_current_month = appointments_current_month.values('anc_type').annotate(count=Count('id'))
#         for item in actual_counts_current_month:
#             anc_type_key = f"ANC_{item['anc_type']}"
#             anc_type_counts_current_month[anc_type_key] = item['count']
#
#         # Filter pregnancy cycles in the last month with status = 0
#         pregnancy_cycles_last_month = PregnancyCycle.objects.filter(
#             delivery_complete_date__range=[first_day_of_last_month, last_day_of_last_month],
#             status=0
#         )
#
#         # Filter pregnancy cycles in the current month with status = 0
#         pregnancy_cycles_current_month = PregnancyCycle.objects.filter(
#             delivery_complete_date__range=[first_day_of_current_month, today],
#             status=0
#         )
#
#         # Count birth_type and delivery_type for last month with status = 0
#         birth_type_counts_last_month = pregnancy_cycles_last_month.values('birth_type').annotate(count=Count('id'))
#         delivery_type_counts_last_month = pregnancy_cycles_last_month.values('delivery_type').annotate(count=Count('id'))
#
#         birth_type_labels = {1: 'still_birth', 2: 'live_birth', 3: 'iud'}
#         delivery_type_labels = {1: 'Normal', 2: 'C_Section'}
#         birth_type_counts_last_month_dict = {label: 0 for label in birth_type_labels.values()}
#         delivery_type_counts_last_month_dict = {label: 0 for label in delivery_type_labels.values()}
#
#         for item in birth_type_counts_last_month:
#             birth_type_key = birth_type_labels[item['birth_type']]
#             birth_type_counts_last_month_dict[birth_type_key] = item['count']
#
#         for item in delivery_type_counts_last_month:
#             delivery_type_key = delivery_type_labels[item['delivery_type']]
#             delivery_type_counts_last_month_dict[delivery_type_key] = item['count']
#
#         # Count birth_type and delivery_type for current month with status = 0
#         birth_type_counts_current_month = pregnancy_cycles_current_month.values('birth_type').annotate(count=Count('id'))
#         delivery_type_counts_current_month = pregnancy_cycles_current_month.values('delivery_type').annotate(count=Count('id'))
#
#         birth_type_counts_current_month_dict = {label: 0 for label in birth_type_labels.values()}
#         delivery_type_counts_current_month_dict = {label: 0 for label in delivery_type_labels.values()}
#
#         for item in birth_type_counts_current_month:
#             birth_type_key = birth_type_labels[item['birth_type']]
#             birth_type_counts_current_month_dict[birth_type_key] = item['count']
#
#         for item in delivery_type_counts_current_month:
#             delivery_type_key = delivery_type_labels[item['delivery_type']]
#             delivery_type_counts_current_month_dict[delivery_type_key] = item['count']
#
#         # Create response data
#         data = {
#             'anc_type_counts_last_month': anc_type_counts_last_month,
#             'anc_type_counts_current_month': anc_type_counts_current_month,
#             'birth_type_counts_last_month': birth_type_counts_last_month_dict,
#             'birth_type_counts_current_month': birth_type_counts_current_month_dict,
#             'delivery_type_counts_last_month': delivery_type_counts_last_month_dict,
#             'delivery_type_counts_current_month': delivery_type_counts_current_month_dict,
#         }
#
#         return Response(data)


class PatientReportDashboardDataAPI(APIView):
    permission_classes = ()
    authentication_classes = (SessionAuthentication,)

    def get(self, request, *args, **kwargs):
        today = datetime.datetime.today()
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        if start_date and end_date:
            format = '%m/%d/%Y'
            start_date = datetime.datetime.strptime(start_date.strip(), format).strftime('%Y-%m-%d')
            end_date = datetime.datetime.strptime(end_date.strip(), format).strftime('%Y-%m-%d')
        else:
            start_date = today.strftime('%Y-%m-%d')
            end_date = today.strftime('%Y-%m-%d')

        # Filter appointments in the given date range
        appointments = Appointment.objects.filter(
            visited_date__range=[start_date, end_date],
            status=1
        )

        if not request.user.is_superuser and request.user.profile.company.type_id == 2:
            company = request.user.profile.company
            appointments = appointments.filter(agency=company)

        # Initialize counts for all ANC types to zero
        anc_type_counts = {f"ANC_{anc_type[0]}": 0 for anc_type in ANC_TYPE}

        # Count appointments by anc_type
        actual_counts = appointments.values('anc_type').annotate(count=Count('id'))
        for item in actual_counts:
            anc_type_key = f"ANC_{item['anc_type']}"
            anc_type_counts[anc_type_key] = item['count']

        # Filter pregnancy cycles in the given date range with status = 0
        pregnancy_cycles = PregnancyCycle.objects.filter(
            delivery_complete_date__range=[start_date, end_date],
            status=0
        )

        # Count birth_type and delivery_type in the given date range with status = 0
        birth_type_counts = pregnancy_cycles.values('birth_type').annotate(count=Count('id'))
        delivery_type_counts = pregnancy_cycles.values('delivery_type').annotate(count=Count('id'))

        birth_type_labels = {1: 'still_birth', 2: 'live_birth', 3: 'iud'}
        delivery_type_labels = {1: 'Normal', 2: 'C_Section'}
        birth_type_counts_dict = {label: 0 for label in birth_type_labels.values()}
        delivery_type_counts_dict = {label: 0 for label in delivery_type_labels.values()}

        for item in birth_type_counts:
            birth_type_key = birth_type_labels[item['birth_type']]
            birth_type_counts_dict[birth_type_key] = item['count']

        for item in delivery_type_counts:
            delivery_type_key = delivery_type_labels[item['delivery_type']]
            delivery_type_counts_dict[delivery_type_key] = item['count']

        # Create response data


        # Count patients with lmp within the date range
        patient_count_with_lmp = PregnancyCycle.objects.filter(
            lmp__range=[start_date, end_date]
        ).values('patient').distinct().count()

        # Count patients with patient_status = 1
        patient_count_with_status = PregnancyCycle.objects.filter(
            patient_status=1
        ).values('patient').distinct().count()

        # Create response data
        data = {
            'anc_type_counts': anc_type_counts,
            'birth_type_counts': birth_type_counts_dict,
            'delivery_type_counts': delivery_type_counts_dict,
            'patient_count_with_lmp': patient_count_with_lmp,
            'patient_count_with_status': patient_count_with_status
        }

        return Response(data)

class BloodCountDataAPI(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = ()

    def get(self, request):
        current_date = timezone.now().date()
        campaign_summery = BloodBank.objects.filter(expiry_date__gt=current_date).values('donor__bloodtype').annotate(count=Count('donor__bloodtype')).order_by()

        if (campaign_summery):
            return Response(campaign_summery)
        return Response(campaign_summery)



