import pymongo


def create_mongodb_collection():
    USER = 'adplay'
    PASSWORD = '~adplay_2020!'
    # url = f"mongodb://{USER}:{PASSWORD}@18.139.183.233:27017/?authSource=admin"
    url = f"mongodb://localhost:27017/"
    print(url)
    mongo_client = pymongo.MongoClient(url)
    adplaydsp = mongo_client["dsp"]
    collist = adplaydsp.list_collection_names()
    print(collist)
    collection_list = [
        {
            'collection': 'bid_request',
            'fields': [
                {'name': 'bidid', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'created_at', 'index': -1},
                {'name': 'app_name', 'index': -1},
                {'name': 'billing_id', 'index': -1},
                {'name': 'timestring', 'index': -1},
                {'name': 'price', 'index': -1},
                {'name': 'impid', 'index': -1},
                {'name': 'crid', 'index': 1},
                {'name': 'w', 'index': 1},
                {'name': 'bidder', 'index': 1},
                {'name': 'bidfloor', 'index': 1},
                {'name': 'type', 'index': 1},
                {'name': 'bids', 'index': 1},
                {'name': 'user_id', 'index': 1},
            ]
        },
        {
            'collection': 'bids',
            'fields': [
                {'name': 'bidid', 'index': -1},
                {'name': 'price', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'crid', 'index': -1},
                {'name': 'billing_id', 'index': -1},
                {'name': 'created_at', 'index': -1},
                {'name': 'bidder', 'index': -1},
            ]
        },
        {
            'collection': 'cookie_files',
            'fields': [
                {'name': 'google_gid', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'bidid', 'index': -1},
                {'name': 'user_list_id', 'index': -1},
                {'name': 'created_at', 'index': -1},
            ]
        },
        {
            'collection': 'impression_details',
            'fields': [
                {'name': 'price', 'index': 1},
                {'name': 'crid', 'index': -1},
                {'name': 'app_name', 'index': 1},
                {'name': 'billing_id', 'index': 1},
                {'name': 'created_at', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'bidder', 'index': 1},
                {'name': 'bidfloor', 'index': 1},
                {'name': 'user_id', 'index': 1},
            ]
        },
        {
            'collection': 'pixel_impression',
            'fields': [
                {'name': 'campaign_id', 'index': -1},
                {'name': 'bidid', 'index': -1},
                {'name': 'created_at', 'index': -1},
            ]
        },
        {
            'collection': 'winnotice',
            'fields': [
                {'name': 'bidid', 'index': -1},
                {'name': 'impid', 'index': -1},
                {'name': 'winprice', 'index': -1},
                {'name': 'created_at', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'billing_id', 'index': -1},
                {'name': 'partner_price', 'index': -1},
                {'name': 'bidder', 'index': 1},
            ]
        },
        {
            'collection': 'clicks',
            'fields': [
                {'name': 'bidid', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'created_at', 'index': -1},
                {'name': 'bidder', 'index': 1},
            ]
        },
        {
            'collection': 'vast_event_tracking',
            'fields': [
                {'name': 'bidid', 'index': -1},
                {'name': 'campaign_id', 'index': -1},
                {'name': 'billing_id', 'index': -1},
                {'name': 'created_at', 'index': -1},
            ]
        }
    ]

    for item in collection_list:
        collection_name = item.get('collection', None)
        fields = item.get('fields', [])
        print(collection_name)
        if collection_name:
            if collection_name in collist:
                mongo_collection = adplaydsp[collection_name]
                mongo_collection.drop()

            created_collection = adplaydsp[collection_name]
            for field in fields:
                field_name = field.get('name', None)
                field_index = field.get('index', 1)
                if field_name:
                    if field_index == 1:
                        created_collection.create_index([(field_name, pymongo.ASCENDING)])
                    else:
                        created_collection.create_index([(field_name, pymongo.DESCENDING)])

