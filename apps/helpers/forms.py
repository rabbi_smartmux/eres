from django.forms import ModelChoiceField, ModelMultipleChoiceField


class TypeModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class ModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class TypeModelChoiceFieldZilla(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.zilla


class TypeModelChoiceFieldUpazila(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.upazila



class TypeModelChoiceFieldUnion(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.union



class TypeModelChoiceFieldWard(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.ward