from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from waitress.utilities import months

from dsp.settings import CURRENT_TEMPLATE


def template(name):
    return CURRENT_TEMPLATE+'/'+name


def timestamp_datetime(timestamp):
    dt_object = datetime.fromtimestamp(timestamp)
    return dt_object

def timestamp_datetime_sql(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
    return dt_object

def this_hour_start_timestamp():
    ct = datetime.now().replace(minute=00,second=00,microsecond=0)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def last_hour_start_timestamp():
    ct = datetime.now().replace(minute=00,second=00,microsecond=0) - timedelta(hours=1)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def selected_hour_ago_start_timestamp(hour):
    ct = datetime.now().replace(minute=00,second=00,microsecond=0) - timedelta(hours=hour)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def prev_hour_start_timestamp():
    ct = datetime.now().replace(minute=00,second=00,microsecond=0)-timedelta(hours=1)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def this_day_start_timestamp():
    ct = datetime.now().replace(hour=0,minute=00,second=00,microsecond=0)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def this_day_start_sql():
    ct = datetime.now().replace(hour=0,minute=00,second=00,microsecond=0).strftime('%Y-%m-%d %H:%M:%S')
    return ct

def sql_datetime_to_timestamp(sql_date_time):
    try:
        ct = datetime.strptime(sql_date_time,'%Y-%m-%d %H:%M:%S')
        return int(ct.timestamp())
    except:
        print('sql_date_time',sql_date_time)
        return 0


def sql_date_to_timestamp(sql_date_time):
    try:
        ct = datetime.strptime(sql_date_time,'%Y-%m-%d')
        return int(ct.timestamp())
    except:
        print('sql_date_time',sql_date_time)
        return 0


def timestamp_day_sql(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).replace(hour=0,minute=00,second=00,microsecond=0).strftime('%Y-%m-%d')
    return dt_object

def timestamp_month_start_sql(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).replace(day=1,hour=0,minute=00,second=00,microsecond=0).strftime('%Y-%m-%d')
    return dt_object

def timestamp_ym_sql(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).replace(day=1,hour=0,minute=00,second=00,microsecond=0).strftime('%Y-%m')
    return dt_object

def timestamp_ymd(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).replace(hour=0,minute=00,second=00,microsecond=0).strftime('%Y%m%d')
    return dt_object

def slq_date_ymd(sql_date):
    dt_object = datetime.strptime(sql_date,'%Y-%m-%d').strftime('%Y%m%d')
    return dt_object

def timestamp_ymd_month_start(timestamp):
    print(timestamp)
    dt_object = datetime.fromtimestamp(timestamp).replace(day=1,hour=0,minute=00,second=00,microsecond=0).strftime('%Y%m')
    return dt_object


def selected_hour_start_timestamp(hour):
    try:
        hour = int(hour)
    except:
        hour = 0
    ct = datetime.now().replace(hour=hour,minute=00,second=00,microsecond=0)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def selected_day_hour_start_timestamp(day,hour):
    try:
        hour = int(hour)
        day = int(day)
    except:
        hour = 0
        day = 1
    ct = datetime.now().replace(day=day,hour=hour,minute=00,second=00,microsecond=0)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def selected_day_start_timestamp(day):
    try:
        day = int(day)
    except:
        day = 1
    ct = datetime.now().replace(day=day,hour=0,minute=00,second=00,microsecond=0)
    print('This Hour Start Time : ',ct)
    return int(ct.timestamp())

def sql_date_datetime_end(date):
    dt_object = datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d 23:59:59')
    return dt_object


def sql_date_datetime_start(date):
    dt_object = datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d 00:00:00')
    return dt_object


def this_month_start_date():
    return datetime.now().replace(day=1).strftime('%Y-%m-%d')

def this_month_end_date():

    return (datetime.now().replace(day=1)+relativedelta(months=1)-timedelta(days=1)).strftime('%Y-%m-%d')

def this_hour_start_sql():
    ct = datetime.now().replace(minute=00,second=00,microsecond=0).strftime('%Y-%m-%d %H:%M:%S')
    return ct


def last_hour_start_sql():
    ct = (datetime.now().replace(minute=00,second=00,microsecond=0)-timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
    return ct

def selected_hour_start_sql(h):
    ct = (datetime.now().replace(minute=00,second=00,microsecond=0)-timedelta(hours=h)).strftime('%Y-%m-%d %H:%M:%S')
    return ct


def selected_hour_date_start_sql(day,h):
    ct = (datetime.now().replace(minute=00,second=00,microsecond=0,day=day,hour=h)).strftime('%Y-%m-%d %H:%M:%S')
    return ct



def int_or_zero(number):
    try:
        return int(number)
    except:
        return 0

def domain_from_url(url):
    print('url : ',url)
    from urllib.parse import urlparse

    parsed_url = urlparse(url)
    domain = parsed_url.netloc

    print('domain : ',domain)  # Output: "www.example.com"
    return domain


def selected_hour_first(hour):
    hour = int_or_zero(hour)
    return datetime.now().replace(hour=hour,minute=00,second=00,microsecond=0).strftime('%Y-%m-%d %H:%M:%S')
def selected_hour_end(hour):
    hour = int_or_zero(hour)
    return datetime.now().replace(hour=hour,minute=59,second=59,microsecond=9999).strftime('%Y-%m-%d %H:%M:%S')


def selected_date_hour_first(day,hour):
    hour = int_or_zero(hour)
    day = int_or_zero(day)
    return datetime.now().replace(day=day,hour=hour,minute=00,second=00,microsecond=0).strftime('%Y-%m-%d %H:%M:%S')
def selected_date_hour_end(day,hour):
    hour = int_or_zero(hour)
    day = int_or_zero(day)
    return datetime.now().replace(day=day,hour=hour,minute=59,second=59,microsecond=9999).strftime('%Y-%m-%d %H:%M:%S')