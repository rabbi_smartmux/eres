from html2image import Html2Image

hti = Html2Image(
    # size=(500, 200),
    custom_flags=['--virtual-time-budget=10000', '--hide-scrollbars']
)

hti.screenshot(url='https://www.python.org', save_as='python_org.png')
