import uuid

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from apps.helpers.utils import STATUS_TYPE


class OperatorStamp(models.Model):
    created_by = models.ForeignKey(User, related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    updated_by = models.ForeignKey(User, related_name='+', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        abstract = True


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False, blank=True, null=True)

    class Meta:
        abstract = True


class CommonModel(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=STATUS_TYPE, default=0)

    class Meta:
        abstract = True


class WithCreatorCommonModel(CommonModel):
    created_by = models.ForeignKey(User, related_name='+', on_delete=models.SET_NULL, blank=True, null=True)
    updated_by = models.ForeignKey(User, related_name='+', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        abstract = True
