from django.db.models import Sum
import datetime
from apps.helpers.helper import timestamp_datetime_sql, timestamp_ym_sql, sql_date_to_timestamp, \
    timestamp_month_start_sql, timestamp_ymd_month_start
from apps.purple_patch.models import CounterHourly, CounterHourlyCurrent, CounterDaily, AdvertiserCampaigns, \
    CounterMonthly, CounterLifetime
from apps.purple_patch.serializers import CounterHourlySerializer, CounterDailySerializer, CounterMonthlySerializer, \
    CounterLifetimeSerializer


def report_adjust(campaign_id,impressions,click,reach,date):
    pass


def insert_or_update_hourly(s_dict):
    ymdh = s_dict.get('ymdh', None)
    ssp_id = str(int(s_dict.get('ssp_id', 0)))
    pub_id = str(int(s_dict.get('pub_id', 0)))
    inv_id = str(int(s_dict.get('inv_id', 0)))
    slot_id = str(int(s_dict.get('slot_id', 0)))
    dsp_id = str(int(s_dict.get('dsp_id', 0)))
    adv_id = str(int(s_dict.get('adv_id', 0)))
    cm_id = str(int(s_dict.get('cm_id', 0)))
    adg_id = str(int(s_dict.get('adg_id', 0)))
    bn_id = str(int(s_dict.get('bn_id', 0)))

    s_dict['ymdh'] = ymdh
    s_dict['ssp_id'] = ssp_id
    s_dict['pub_id'] = pub_id
    s_dict['inv_id'] = inv_id
    s_dict['slot_id'] = slot_id
    s_dict['dsp_id'] = dsp_id
    s_dict['adv_id'] = adv_id
    s_dict['cm_id'] = cm_id
    s_dict['adg_id'] = adg_id
    s_dict['bn_id'] = bn_id

    s_dict['bidprice_bm'] = round(eval(s_dict.get('bidprice_bm', 0)), 4)
    s_dict['bidprice_rl'] = round(eval(s_dict.get('bidprice_rl', 0)), 4)
    s_dict['winprice'] = round(eval(s_dict.get('winprice', 0)), 4)
    s_dict['spend'] = round(eval(s_dict.get('spend', 0)), 4)
    s_dict['earning'] = round(eval(s_dict.get('earning', 0)), 4)

    # if ymdh:
    #     ymdh = timestamp_datetime_sql(ymdh)
    # else:
    #     ymdh = this_hour_start_timestamp()

    try:
        ymdh = timestamp_datetime_sql(ymdh)
    except:
        pass

    s_dict['ymdh'] = ymdh
    s_dict['key'] = hourly_key_gen(s_dict)
    print(s_dict['key'])
    hourly_count = CounterHourly.objects.filter(key=s_dict['key']).first()
    print('hourly_count', hourly_count)

    if hourly_count:
        serializer_daily = CounterHourlySerializer(data=s_dict, instance=hourly_count)
    else:
        serializer_daily = CounterHourlySerializer(data=s_dict, many=False)
    # print(serializer_daily)
    serializer_daily.is_valid(raise_exception=True)
    serializer_daily.save()


def hourly_key_gen(s_dict):
    print(s_dict)
    ymdh = s_dict.get('ymdh', None)
    ssp_id = str(int(s_dict.get('ssp_id', None)))
    pub_id = str(int(s_dict.get('pub_id', None)))
    inv_id = str(int(s_dict.get('inv_id', None)))
    slot_id = str(int(s_dict.get('slot_id', None)))
    dsp_id = str(int(s_dict.get('dsp_id', None)))
    adv_id = str(int(s_dict.get('adv_id', None)))
    cm_id = str(int(s_dict.get('cm_id', None)))
    adg_id = str(int(s_dict.get('adg_id', None)))
    bn_id = str(int(s_dict.get('bn_id', None)))
    # ymdh = timestamp_datetime_sql(ymdh)

    h_list = [ymdh, ssp_id, pub_id, inv_id, slot_id, dsp_id, adv_id, cm_id, adg_id, bn_id]

    print(h_list)

    response = ':'.join(h_list)
    print(response)
    return response

def insert_or_update_daily(date, ymd,cm_id=None):
    print('Date', date)
    counter_hourly_current = CounterHourlyCurrent.objects.filter(ymdh__contains=date)
    if cm_id:
        counter_hourly_current = counter_hourly_current.filter(cm_id=cm_id)
    count_hourly = (
    counter_hourly_current.values('ssp_id', 'pub_id', 'inv_id', 'slot_id',
                                                                    'dsp_id', 'adv_id', 'cm_id', 'adg_id', 'bn_id')
    .order_by().annotate(
        Sum('hits'),
        Sum('bots'),
        Sum('requests'),
        Sum('errors'),
        Sum('fraud'),
        Sum('passback'),
        Sum('bids'),
        Sum('wins'),
        Sum('pixels'),
        Sum('visible'),
        Sum('clicks'),
        Sum('bidprice_bm'),
        Sum('bidprice_rl'),
        Sum('winprice'),
        Sum('spend'),
        Sum('earning')
    ))
    print(count_hourly.count(), count_hourly)
    for item in count_hourly:
        print(date, item)
        key_list = [ymd, item.get('ssp_id'), item.get('pub_id'), item.get('inv_id'), item.get('slot_id'),
                    item.get('dsp_id'), item.get('adv_id'), item.get('cm_id'), item.get('adg_id'),
                    item.get('bn_id')]
        key = ':'.join(key_list)
        # item['earning'] = round(item.get('earning__sum'),4)

        item['key'] = key
        item['ymd'] = date
        item['hits'] = round(item.get('hits__sum'))
        item['bots'] = round(item.get('bots__sum'))
        item['requests'] = round(item.get('requests__sum'))
        item['errors'] = round(item.get('errors__sum'))
        item['fraud'] = round(item.get('fraud__sum'))
        item['passback'] = round(item.get('passback__sum'))
        item['bids'] = round(item.get('bids__sum'))
        item['wins'] = round(item.get('wins__sum'))
        item['pixels'] = round(item.get('pixels__sum'))
        item['visible'] = round(item.get('visible__sum'))
        item['clicks'] = round(item.get('clicks__sum'))
        item['bidprice_bm'] = round(item.get('bidprice_bm__sum'), 4)
        item['bidprice_rl'] = round(item.get('bidprice_rl__sum'), 4)
        item['winprice'] = round(item.get('winprice__sum'), 4)
        item['spend'] = round(item.get('spend__sum'), 4)
        item['earning'] = round(item.get('earning__sum'), 4)
        print(item)

        daily_counter = CounterDaily.objects.filter(key=key).first()
        if daily_counter:
            daily_serializer = CounterDailySerializer(data=item, instance=daily_counter)
        else:
            daily_serializer = CounterDailySerializer(data=item, many=False)

        daily_serializer.is_valid(raise_exception=True)
        daily_serializer.save()
        print('Saved Daily Counter')

def insert_or_update_monthly(sql_date,cm_id=None):
    hour_start = sql_date_to_timestamp(sql_date)
    date = day_start = timestamp_month_start_sql(hour_start)
    ymd = day_ymd = timestamp_ymd_month_start(hour_start)
    ym = timestamp_ym_sql(hour_start)

    if not cm_id:
        return False
    # running_campaign_id = AdvertiserCampaigns.objects.filter(campaign_start_date__lte=now_time,
    #                                                          campaign_end_date__gte=now_time).values_list('campaign_id', flat=True)
    count_hourly = (
    CounterDaily.objects.filter(ymd__contains=ym,cm_id=cm_id).values('ssp_id', 'pub_id', 'inv_id', 'slot_id',
                                                                    'dsp_id', 'adv_id', 'cm_id', 'adg_id', 'bn_id')
    .order_by().annotate(
        Sum('hits'),
        Sum('bots'),
        Sum('requests'),
        Sum('errors'),
        Sum('fraud'),
        Sum('passback'),
        Sum('bids'),
        Sum('wins'),
        Sum('pixels'),
        Sum('visible'),
        Sum('clicks'),
        Sum('bidprice_bm'),
        Sum('bidprice_rl'),
        Sum('winprice'),
        Sum('spend'),
        Sum('earning')
    ))
    print(count_hourly.count(), count_hourly)
    for item in count_hourly:
        print(date, item)
        key_list = [ymd, item.get('ssp_id'), item.get('pub_id'), item.get('inv_id'), item.get('slot_id'),
                    item.get('dsp_id'), item.get('adv_id'), item.get('cm_id'), item.get('adg_id'),
                    item.get('bn_id')]
        key = ':'.join(key_list)
        # item['earning'] = round(item.get('earning__sum'),4)

        item['key'] = key
        item['ym'] = date
        item['hits'] = round(item.get('hits__sum'))
        item['bots'] = round(item.get('bots__sum'))
        item['requests'] = round(item.get('requests__sum'))
        item['errors'] = round(item.get('errors__sum'))
        item['fraud'] = round(item.get('fraud__sum'))
        item['passback'] = round(item.get('passback__sum'))
        item['bids'] = round(item.get('bids__sum'))
        item['wins'] = round(item.get('wins__sum'))
        item['pixels'] = round(item.get('pixels__sum'))
        item['visible'] = round(item.get('visible__sum'))
        item['clicks'] = round(item.get('clicks__sum'))
        item['bidprice_bm'] = round(item.get('bidprice_bm__sum'), 4)
        item['bidprice_rl'] = round(item.get('bidprice_rl__sum'), 4)
        item['winprice'] = round(item.get('winprice__sum'), 4)
        item['spend'] = round(item.get('spend__sum'), 4)
        item['earning'] = round(item.get('earning__sum'), 4)
        print(item)

        # if int(item['cm_id'])==1733:
        #     print(item['cm_id'])

        daily_counter = CounterMonthly.objects.filter(key=key).first()
        print('Monthly Counter Ex',daily_counter)
        if daily_counter:
            daily_serializer = CounterMonthlySerializer(data=item, instance=daily_counter)
        else:
            daily_serializer = CounterMonthlySerializer(data=item, many=False)

        daily_serializer.is_valid(raise_exception=True)
        daily_serializer.save()
        print('Saved Daily Counter')

def insert_or_update_lifetime(sql_date,cm_id=None):
    # print('Date', date)
    hour_start = sql_date_to_timestamp(sql_date)
    day_start = timestamp_month_start_sql(hour_start)
    day_ymd = timestamp_ymd_month_start(hour_start)
    ym = timestamp_ym_sql(hour_start)

    # running_campaign_id = AdvertiserCampaigns.objects.filter(campaign_start_date__lte=now_time,campaign_end_date__gte=now_time).values_list('campaign_id',flat=True)
    count_hourly = (
    CounterDaily.objects.filter(ymd__contains=ym,cm_id=cm_id).values('ssp_id', 'pub_id', 'inv_id', 'slot_id',
                                                                    'dsp_id', 'adv_id', 'cm_id', 'adg_id', 'bn_id')
    .order_by().annotate(
        Sum('hits'),
        Sum('bots'),
        Sum('requests'),
        Sum('errors'),
        Sum('fraud'),
        Sum('passback'),
        Sum('bids'),
        Sum('wins'),
        Sum('pixels'),
        Sum('visible'),
        Sum('clicks'),
        Sum('bidprice_bm'),
        Sum('bidprice_rl'),
        Sum('winprice'),
        Sum('spend'),
        Sum('earning')
    ))
    print(count_hourly.count(), count_hourly)
    for item in count_hourly:

        key_list = [item.get('ssp_id'), item.get('pub_id'), item.get('inv_id'), item.get('slot_id'),
                    item.get('dsp_id'), item.get('adv_id'), item.get('cm_id'), item.get('adg_id'),
                    item.get('bn_id')]
        key = ':'.join(key_list)
        # item['earning'] = round(item.get('earning__sum'),4)

        item['key'] = key
        item['hits'] = round(item.get('hits__sum'))
        item['bots'] = round(item.get('bots__sum'))
        item['requests'] = round(item.get('requests__sum'))
        item['errors'] = round(item.get('errors__sum'))
        item['fraud'] = round(item.get('fraud__sum'))
        item['passback'] = round(item.get('passback__sum'))
        item['bids'] = round(item.get('bids__sum'))
        item['wins'] = round(item.get('wins__sum'))
        item['pixels'] = round(item.get('pixels__sum'))
        item['visible'] = round(item.get('visible__sum'))
        item['clicks'] = round(item.get('clicks__sum'))
        item['bidprice_bm'] = round(item.get('bidprice_bm__sum'), 4)
        item['bidprice_rl'] = round(item.get('bidprice_rl__sum'), 4)
        item['winprice'] = round(item.get('winprice__sum'), 4)
        item['spend'] = round(item.get('spend__sum'), 4)
        item['earning'] = round(item.get('earning__sum'), 4)
        print(item)

        # if int(item['cm_id'])==1733:
        #     print(item['cm_id'])

        daily_counter = CounterLifetime.objects.filter(key=key).first()
        print('Monthly Counter Ex',daily_counter)
        if daily_counter:
            daily_serializer = CounterLifetimeSerializer(data=item, instance=daily_counter)
        else:
            daily_serializer = CounterLifetimeSerializer(data=item, many=False)

        daily_serializer.is_valid(raise_exception=True)
        daily_serializer.save()
        print('Saved Daily Counter')

