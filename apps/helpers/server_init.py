from apps.accounts.models import CompanyType, Company


def set_company_type():
    data =  ['AdTech','Publishers','Brand','Agency']
    if not CompanyType.objects.all().count():
        for item in data:
            try:
                CompanyType.objects.create(name=item)
            except:
                pass

def set_initial_companies():
    data = [{'name':'Purple Patch','type':1,'code':'C-1001'},{'name':'Rabbithole BD','type':2}]
    if not Company.objects.all().count():
        for item in data:
            try:
                Company.objects.create(name=item.get('name'),type_id=item.get('type'))
            except:
                pass