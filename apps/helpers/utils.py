STATUS_TYPE = (
    (1, 'Active'),
    (0, 'Inactive'),
)

PregnancyCycle_TYPE = (
    (1, 'Open'),
    (0, 'Closed'),
)

DELIVERY_TYPE = (
    (0, '-----'),
    (1, 'Normal'),
    (2, 'C Section'),
)

BIRTH_TYPE = (
    (0, '-----'),
    (1, 'Still Birth'),
    (2, 'Live Birth'),
    (2, 'IUD'),
)


DELIVERY_CENTER = (
    (1, 'Home'),
    (0, 'Hospital'),
)

VISIT_TYPE = (
    (1, 'Visited'),
    (0, 'Not Visited'),
)

ANC_TYPE = (
    (1, 'ANC 1'),
    (2, 'ANC 2'),
    (3, 'ANC 3'),
    (4, 'ANC 4'),
)

CURRENCY = (
    ('BDT', 'BDT'),
    ('USD', 'USD'),
)

GOAL_TYPE = (
    ('CPM', 'CPM'),
    ('CPC', 'CPC'),
    ('CPV', 'CPV'),
    ('CPCV', 'CPCV'),
)

NETWORK_TYPE = (
    ('Native', 'Native'),
    ('Programmatic', 'Programmatic'),
)

AD_NETWORKS = (
    ('Adx', 'Adx'),
    ('PP', 'Purple Patch'),
)

OBJECTIVE_TYPE = (
    ('Impression', 'Impression'),
    ('Reach', 'Reach'),
)

CAMPAIGN_TYPE = (
    (1, 'Banner'),
    (101, 'HTML'),
    (201, 'Native'),
    (301, 'Video'),
    (302, 'Vast'),
    # (303, 'VPAID'),
    # (501, 'Carousel'),
    # (601, 'Scroller'),
)

LIFE_STYLE = (
    (1, 'Drinking Alcohol'),
    (2, 'smoking'),
)

HEALTH_PPROBLEM = (
    (1, 'BP'),
    (2, 'Diabetes'),
    (3, 'Obesity'),
    (4, 'Thyroid'),
    (5, 'Heart Problem'),
    (6, 'Bronchial Asthma'),
    (7, 'kidney Problem'),
    (8, 'Epilepsy'),
)

POSITION_TYPE = (
    (1, 'Static'),
    (2, 'Floating'),
    (3, 'Popup'),
    (4, 'In-Video'),
)

LOGO_PLACEMENT = (
    (1, 'Top Left'),
    (2, 'Top Right'),
    (3, 'Bottom Left'),
    (4, 'Bottom Right'),
)

LOCATION_TYPE = (
    (1, 'GEP Point'),
    (2, 'GEO Fencing')
)

KPI_RULES = (
    ('Winrate', 'Winrate'),
    ('CTR', 'CTR'),
    ('Viewability', 'Viewability'),
    ('Renderrate', 'Renderrate'),
)

CREATIVE_STATUS = (
    (0, 'Pending'),
    (1, 'Approved'),
    (2, 'Rejected'),
)

CAMPAIGN_STATUS = (
    (0, 'Pending'),
    (1, 'Approved'),
    (2, 'Rejected'),
)

PLATFORM_TYPE = (
    ('CPM', 'CPM'),
    ('CPC', 'CPC'),
    ('CPI', 'CPI'),
    ('CPA', 'CPA'),
    ('CPV', 'CPV'),
    ('CPCV', 'CPCV'),
)

GOAL_TYPE_2 = (
    ('Impression', 'Impression'),
    ('Click', 'Click'),
    ('View', 'View'),
)

INVOICE_STATUS = (
    (0, 'Pending'),
    (1, 'Approved'),
    (2, 'Rejected'),
)

GOAL_TYPE_2 = (
    ('Impression', 'Impression'),
    ('Click', 'Click'),
    ('View', 'View'),
)