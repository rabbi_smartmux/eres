function PAMS(code,type)
{
  let theUrl = 'https://www.cloudflare.com/cdn-cgi/trace';
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    let data = xmlHttp.responseText;
    data = data.trim().split('\n').reduce(function(obj, pair) {
      pair = pair.split('=');
      return obj[pair[0]] = pair[1], obj;
    }, {});
    data['url'] = window.location.href;
    data['code'] = code;
    data['type'] = type;
    data['nid'] = '60b4a817bbf01';
    data['site_cookie'] = document.cookie;
    let cookie_data = getCookies();
    let return_data = {...data,...cookie_data};
    let query_string = param( return_data );

    let link = 'https://ads.purplepatch.online/timp?'+query_string;
    const image = document.createElement('img')
    image.src  = link;
    image.style = 'display:none';
    document.body.appendChild(image);
    return return_data;
}

function PAMSA(code,type)
{
  let theUrl = 'https://www.cloudflare.com/cdn-cgi/trace';
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
  xmlHttp.send( null );
  let data = xmlHttp.responseText;
  data = data.trim().split('\n').reduce(function(obj, pair) {
    pair = pair.split('=');
    return obj[pair[0]] = pair[1], obj;
  }, {});
  data['url'] = document.referrer;
  data['code'] = code;
  data['type'] = type;
  data['nid'] = '60b4a817bbf01';
  data['site_cookie'] = document.cookie;
  let cookie_data = getCookies();
  let return_data = {...data,...cookie_data};
  let query_string = param( return_data );

  let link = 'https://ads.purplepatch.online/timp?'+query_string;
  const image = document.createElement('img')
  image.src  = link;
  image.style = 'display:none';
  document.body.appendChild(image);
  return return_data;
}




function param(data){
  url = Object.keys(data).map(function(k) {
    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
}).join('&');
  return url;
}




function getCookies(){
  var pairs = document.cookie.split(";");
  var cookies = {};
  for (var i=0; i<pairs.length; i++){
    var pair = pairs[i].split("=");
    cookies[(pair[0]+'').trim()] = unescape(pair.slice(1).join('='));
  }
  return cookies;
}

function ButtonTrack(){
  var code = this.getAttribute("pams-pid");
  var type = this.getAttribute("pams-itype");
  PAMS(code,type)
}




document.addEventListener('DOMContentLoaded', function() {
  console.log('document is ready. I can sleep now');
  var atdbuttonelement = document.getElementsByClassName("pams-btn-t");

  Array.from(atdbuttonelement).forEach(function(button) {
    button.addEventListener('click', ButtonTrack);
  });
});