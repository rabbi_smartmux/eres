let adsURL = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4';
let playerWrapper = document.getElementById('player-wrapper');
let WatchBtn = document.getElementsByClassName('watch-btn')[0]
if (WatchBtn){
    WatchBtn.addEventListener('click', () => {
        setVideoAds()
    })
}
if (playerWrapper){
    setVideoAds()
}

function setVideoAds() {
    adsURL = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4';

    setTimeout(() => {
        playerWrapper = document.getElementById('player-wrapper');
        if (playerWrapper) {
            let PlayerContainerID = 'react-player'
            let videoPlayerContainer = document.getElementById(PlayerContainerID)
            let videoPlayer = videoPlayerContainer.getElementsByTagName('video')[0]
            let videoAdsPlayer = document.getElementById('pp_vast')
            var skipb = document.getElementById("skip_btn");
            var controls = document.getElementById('player-control-section');


            var adplaying = false;
            var skip_clicked = false;
            var adstart = 0;

            console.log(videoPlayer, videoAdsPlayer);
            if (videoPlayer) {
                console.log(videoPlayer.width, videoPlayer.height, videoPlayerContainer.offsetHeight, videoPlayerContainer.offsetWidth)
                videoPlayerContainer.style.position = 'relative';
                videoPlayerContainer.style.display = 'flex';
                if (videoAdsPlayer) {

                } else {
                    const videoAds = document.createElement('video');
                    videoAds.src = adsURL;
                    videoAds.id = 'pp_vast'
                    videoAds.style = "display:none;width: 100%; height: 100%;";
                    videoPlayerContainer.appendChild(videoAds)
                    // videoAds.muted = true;
                    // videoAds.style.display = 'block';
                    // videoAds.play();
                    videoAdsPlayer = document.getElementById('pp_vast')

                    const skipbtn = document.createElement('button')
                    skipbtn.id = 'skip_btn'
                    skipbtn.className = 'skip_btn'
                    skipbtn.textContent = 'Skip Ad'
                    skipbtn.style = 'position: absolute; bottom: 10px; padding: 7px; right: 10px;border: 1px solid rgba(255,255,255,.5);box-shadow: 1px 1px 1px #ccc; opacity: .4;background: rgba(0,0,0,.7);    color: #fff;'
                    videoPlayerContainer.appendChild(skipbtn)
                    skipb = document.getElementById("skip_btn");
                }

                videoAdsPlayer.onended = function () {
                    videoPlayer.style.display = 'block';
                    videoAdsPlayer.style.display = 'none';
                    videoPlayer.play()
                }


                // videoPlayer.style.display = 'none';

                videoPlayer.onplay = function () {
                    console.log('aaa')
                    controls.style.display = 'block';
                }


                var mid = Math.round(videoPlayer.duration/(5/2))
                var end = Math.round(videoPlayer.duration/(5/4))
                var adsTimes = [3,mid,end]

                console.log(adsTimes);

                videoPlayer.addEventListener("timeupdate", () => {
                    // console.log(videoPlayer.currentTime,'Current Time');
                    let curr = (videoPlayer.currentTime / videoPlayer.duration) * 100
                    curr = videoPlayer.currentTime;
                    if (videoPlayer.ended) {
                        document.querySelector(".fa-play").style.display = "block"
                        document.querySelector(".fa-pause").style.display = "none"
                    }
                    // document.querySelector('.inner').style.width = `${curr}%`
                    // console.log(curr);
                    // console.log(videoPlayer.style.display)
                    var vid1play = videoPlayer.style.display != 'none';
                    // console.log(vid1play)

                    if (adsTimes.includes(Math.round(curr)) && vid1play && !adplaying && !skip_clicked) {
                        vid1play = true;
                        adplaying = true;
                        adstart = Math.round(curr);
                        console.log('Ad Play Time Updated', adstart, curr)

                        videoAdsPlayer.play();
                        videoPlayer.pause();
                        videoPlayer.style.display = "none";
                        controls.style.display = 'none';
                        skipb.style.display = "block";
                        videoAdsPlayer.style.display = "block";
                    }

                    if (!adplaying) {
                        skipb.style.display = "none";
                    }

                    console.log(adplaying, !adplaying, adstart + 2, Math.round(curr), Math.round(curr) > adstart + 2);
                    if (Math.round(curr) > adstart + 2) {
                        adplaying = false;
                        skip_clicked = false;
                    }
                })

                skipb.addEventListener('click', () => {
                    videoAdsPlayer.onended()
                    videoAdsPlayer.currentTime = 0; //Just to illusrate as begining is black screen
                    videoAdsPlayer.pause();
                    skipb.style.display = "none";
                    adplaying = false;
                    skip_clicked = true;
                })
            }
        }

    }, 500);


}