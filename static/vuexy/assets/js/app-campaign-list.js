/**
 * Page User List
 */

'use strict';

// Datatable (jquery)
$(function () {
    let borderColor, bodyBg, headingColor;

    if (isDarkStyle) {
        borderColor = config.colors_dark.borderColor;
        bodyBg = config.colors_dark.bodyBg;
        headingColor = config.colors_dark.headingColor;
    } else {
        borderColor = config.colors.borderColor;
        bodyBg = config.colors.bodyBg;
        headingColor = config.colors.headingColor;
    }

    // Variable declaration for table
    var dt_user_table = $('.datatables-users'),
        select2 = $('.select2'),
        userView = 'app-user-view-account.html',
        statusObj = {
            1: {title: 'Pending', class: 'bg-label-warning'},
            2: {title: 'Active', class: 'bg-label-success'},
            3: {title: 'Inactive', class: 'bg-label-secondary'}
        };

    if (select2.length) {
        var $this = select2;
        $this.wrap('<div class="position-relative"></div>').select2({
            placeholder: 'Select Country',
            dropdownParent: $this.parent()
        });
    }

    // Users datatable
    if (dt_user_table.length) {
        var dt_user = dt_user_table.DataTable({
            ajax: '/api/v1/campaign/?format=datatables', // JSON file to add data
            processing: true,
            serverSide: true,
            columns: [
                // columns according to JSON
                {
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    data: '',
                    mRender: function (data, type, row) {
                        console.log(data)
                        return '';
                    }
                },
                {data: 'name'},
                {data: 'agency_name', name: 'agency.name'},
                {data: 'platform_name', name: 'platform.name'},
                {data: 'campaign_type'},
                {data: 'start_date'},
                {data: 'end_date'},
                {
                    data: 'id',
                    mRender: function (data, type, row) {
                        let userView = '';
                        console.log(data);
                        let url = `/campaign/${data}/edit`;
                        let url_details = `/campaign/${data}/detail`;
                        console.log(url);
                        let delete_extra = `<a href="javascript:;" class="text-body delete-record" data-id="${data}"><i class="ti ti-trash ti-sm mx-2"></i></a>
                           
                            <div class="dropdown-menu dropdown-menu-end m-0">
                            </div>`;
                        if (sadm=='False' && staff == 'False'){
                            delete_extra = `<div></div>`
                        }
                        let response = `<div class="d-flex align-items-center">
                            <a href="javascript:;"  data-url="${url_details}" class="text-body user-edit" data-size="modal-xl" data-id="${data}"><i class="ti ti-eye ti-sm me-2"></i></a>
                            <a href="javascript:;"  data-url="${url}" class="text-body user-edit" data-id="${data}"><i class="ti ti-edit ti-sm me-2"></i></a>
                            ${delete_extra}
                            </div>`
                        return response;

                    }
                }
            ],
            order: [[1, 'desc']],
            dom:
                '<"row me-2"' +
                '<"col-md-2"<"me-3"l>>' +
                '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                '>t' +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            language: {
                sLengthMenu: '_MENU_',
                search: '',
                searchPlaceholder: 'Search..'
            },
            // Buttons with Dropdown
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-label-secondary dropdown-toggle mx-3',
                    text: '<i class="ti ti-screen-share me-1 ti-xs"></i>Export',
                    buttons: [
                        {
                            extend: 'print',
                            text: '<i class="ti ti-printer me-2" ></i>Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be print
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            },
                            customize: function (win) {
                                //customize print view for dark
                                $(win.document.body)
                                    .css('color', headingColor)
                                    .css('border-color', borderColor)
                                    .css('background-color', bodyBg);
                                $(win.document.body)
                                    .find('table')
                                    .addClass('compact')
                                    .css('color', 'inherit')
                                    .css('border-color', 'inherit')
                                    .css('background-color', 'inherit');
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<i class="ti ti-file-text me-2" ></i>Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="ti ti-file-code-2 me-2"></i>Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'copy',
                            text: '<i class="ti ti-copy me-2" ></i>Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    text: '<i class="ti ti-plus me-0 me-sm-1 ti-xs"></i><span class="d-none d-sm-inline-block">Add New Campaign</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'id': 'create_campaign',
                        // 'data-bs-toggle': 'offcanvas',
                        // 'data-bs-target': '#offcanvasAddUser'
                    }
                }
            ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/><tbody />').append(data) : false;
                    }
                }
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Type </option></select>'
                        )
                            .appendTo('.user_role')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         select.append('<option value="' + d + '">' + d + '</option>');
                        //     });
                    });
                // Adding plan filter once table initialized
                // this.api()
                //     .columns(3)
                //     .every(function () {
                //         var column = this;
                //         var select = $(
                //             '<select id="UserPlan" class="form-select text-capitalize"><option value=""> Select Plan </option></select>'
                //         )
                //             .appendTo('.user_plan')
                //             .on('change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex($(this).val());
                //                 column.search(val ? '^' + val + '$' : '', true, false).draw();
                //             });
                //
                //         // column
                //         //     .data()
                //         //     .unique()
                //         //     .sort()
                //         //     .each(function (d, j) {
                //         //         select.append('<option value="' + d + '">' + d + '</option>');
                //         //     });
                //     });
                // Adding status filter once table initialized
                this.api()
                    .columns(5)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="FilterTransaction" class="form-select text-capitalize"><option value=""> Select Status </option></select>'
                        )
                            .appendTo('.user_status')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         // select.append(
                        //         //     '<option value="' +
                        //         //     statusObj[d].title +
                        //         //     '" class="text-capitalize">' +
                        //         //     statusObj[d].title +
                        //         //     '</option>'
                        //         // );
                        //     });
                    });
            }
        });
    }

    // Delete Record
    $('html').on('click', '.delete-record', function () {
        $('.modal').find('[aria-label="Close"]').click();
        let id = $(this).attr('data-id');
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }


        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary me-3',
                cancelButton: 'btn btn-label-secondary'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                axios.delete(`/api/v1/campaign/${id}/`)
                    .then(function (response) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            timer: 1500,
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        dt_user.ajax.reload();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'Your imaginary file is safe :)',
                    icon: 'error',
                    timer: 1500,
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });

    });

    $('html').on('click', '.status_change', function () {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let status = $(this).attr('data-status');
        let id = $(this).attr('data-id');
        axios.put(`/api/v1/company/${id}/`, {status: status})
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $('html').on('click', '#create_campaign', function () {
        let url = '/campaign/create';
        console.log(url)
        $('#ajaxModal').modal('show');
        $('#ajaxModal').find('#modal-content').load(url, function () {
            $('#ajaxModal').find('form').attr('action', url)
            $('#ajaxModal').find('#close_modal').attr('data-bs-dismiss', 'modal')
            $('#ajaxModal').find('#close_modal').attr('aria-label', "Close")
            $('#ajaxModal').find('select').select2({dropdownParent: $("#ajaxModal")});
        })
    });

    $('html').on('click', '.user-edit', function () {
        let url = $(this).attr('data-url');
        let id = $(this).attr('data-id');
        let size = $(this).attr('data-size');
        if (!size) {
            size = 'modal-lg'
        }
        console.log(url)
        $(".modal:not(#ajaxModal)").find('[aria-label="Close"]').click();
        $('#ajaxModal').find('.modal-dialog').removeClass('modal-lg').removeClass('modal-xl').addClass(size)
        $('#ajaxModal').modal('show');

        $('#ajaxModal').find('#modal-content').load(url, function () {
            $('#ajaxModal').find('form').attr('action', url)
            $('#ajaxModal').find('form').attr('data-id', id)
            $('#ajaxModal').find('#close_modal').attr('data-bs-dismiss', 'modal')
            $('#ajaxModal').find('#close_modal').attr('aria-label', "Close")
            $('#ajaxModal').find('select').select2({dropdownParent: $("#ajaxModal")});
            $('input.timeinput').timepicker({
                timeFormat: 'H:i:s',
                interval: 15
            });
            const id_sites = document.querySelector('#id_sites'), Tagifyid_sites = new Tagify(id_sites);
            const id_apps = document.querySelector('#id_apps'), Tagifyid_apps = new Tagify(id_apps);

            $('#div_id_file').closest('.form-group').hide()
            $('#id_html_tag').closest('.form-group').hide()
            open_publisher_status()
            frequency_capping_status()

        })
    });

// campaign_form

    $('html').on('submit', '#campaign_form', function (e) {
        e.preventDefault();
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        console.log(form_data);
        let id = $(this).attr('data-id');
        if (id) {
            axios.put(`/api/v1/campaign/${id}/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });

        } else {
            axios.post(`/api/v1/campaign/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    });

    $('html').on('submit', '#campaign_inventory_form', function (e) {
        e.preventDefault();
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        $('select[multiple]').each(function () {
            form_data[$(this).attr('name')] = $(this).val()
        })
        $('input[type="checkbox"]').each(function () {
            form_data[$(this).attr('name')] = $(this).is(":checked")
        })

        console.log(form_data);
        let id = $(this).attr('data-id');
        if (id) {
            axios.put(`/api/v1/campaign_inventory/${id}/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });

        } else {
            axios.post(`/api/v1/campaign_inventory/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    });

    $('html').on('submit', '#campaign_targeting_form', function (e) {
        e.preventDefault();
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        $('select[multiple]').each(function () {
            form_data[$(this).attr('name')] = $(this).val()
        })
        console.log(form_data);
        let id = $(this).attr('data-id');
        if (id) {
            axios.put(`/api/v1/campaign_targets/${id}/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });

        } else {
            axios.post(`/api/v1/campaign_targets/`, form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('#ajaxModal').find('[aria-label="Close"]').click()
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    });

    let open_publisher_status = function () {
        let open_publisher = $('[name="open_publisher"]').is(':checked')
        console.log(open_publisher)
        if (open_publisher) {
            $('#div_id_sites').hide().find('input').val('')
            $('#div_id_apps').hide().find('input').val('')
        } else {
            $('#div_id_sites').show()
            $('#div_id_apps').show()
        }
    }

    let frequency_capping_status = function () {
        let frequency_capping = $('[name="frequency_capping"]').is(':checked')
        if (frequency_capping){
            $('#div_id_capping_per_day').show()
        }else {
            $('#div_id_capping_per_day').hide().find('input').val(0)
        }
    }

    $('html').on('change', '[name="open_publisher"]', function () {
        open_publisher_status()
    })

    $('html').on('change', '[name="frequency_capping"]', function () {
        frequency_capping_status()
    })


    $('html').on('submit', '#campaign_creative_form', function (e) {
        e.preventDefault();
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        const form = document.querySelector("form#campaign_creative_form");
        const form_data = new FormData(form);
        // let form_data = objectifyForm($(this).serializeArray());
        // $('select[multiple]').each(function () {
        //     form_data[$(this).attr('name')] = $(this).val()
        // })
        console.log(form_data);
        let id = $(this).attr('data-id');

        axios.post(`/api/v1/campaign_creatives/`, form_data, config)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('#ajaxModal').find('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error);
            });


    });


    $('html').on('submit', '#editUserForm', function () {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        console.log(form_data);
        let id = $(this).attr('data-id');
        axios.put(`/api/v1/company/${id}/`, form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('#ajaxModal').find('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error);
            });
    });
    $('html').on('click', '#submit_data', function () {
        // {#$('#user_creation_form_container').load('{% url "user_create" %}')#}
        let form_data = objectifyForm($('form#addNewUserForm').serializeArray());
        // {#const headers = {"X-CSRFTOKEN": "{% csrf_token() %}"}#}
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        axios.post("/api/v1/company/", form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('html').find('[aria-label="Close"]').click();
            })
            .catch(function (error) {
                console.log(error);
            });

    });

    // Filter form control to default size
    // ? setTimeout used for multilingual table initialization
    setTimeout(() => {
        $('.dataTables_filter .form-control').removeClass('form-control-sm');
        $('.dataTables_length .form-select').removeClass('form-select-sm');
    }, 300);
});

// Validation & Phone mask
(function () {
    const phoneMaskList = document.querySelectorAll('.phone-mask'),
        addNewUserForm = document.getElementById('addNewUserForm');

    // Phone Number
    if (phoneMaskList) {
        phoneMaskList.forEach(function (phoneMask) {
            new Cleave(phoneMask, {
                phone: true,
                phoneRegionCode: 'US'
            });
        });
    }
    // Add New User Form Validation
    const fv = FormValidation.formValidation(addNewUserForm, {
        fields: {
            userFullname: {
                validators: {
                    notEmpty: {
                        message: 'Please enter fullname '
                    }
                }
            },
            userEmail: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({
                // Use this for enabling/changing valid/invalid class
                eleValidClass: '',
                rowSelector: function (field, ele) {
                    // field is the field name & ele is the field element
                    return '.mb-3';
                }
            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
})();
