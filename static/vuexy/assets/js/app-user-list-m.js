/**
 * Page User List
 */

'use strict';

// Datatable (jquery)
$(function () {
    let borderColor, bodyBg, headingColor;
    let add_perms = document.getElementById('add_perms');
    console.log(add_perms)
    let add_button = {}
    if (add_perms) {
        add_button = {
            text: '<i class="ti ti-plus me-0 me-sm-1 ti-xs"></i><span class="d-none d-sm-inline-block">Add New User</span>',
            className: 'add-new btn btn-primary',
            attr: {
                'id': 'createUser',
                'data-bs-toggle': 'offcanvas',
                // 'data-bs-target': '#offcanvasAddUser'
            }
        }
    }

    if (isDarkStyle) {
        borderColor = config.colors_dark.borderColor;
        bodyBg = config.colors_dark.bodyBg;
        headingColor = config.colors_dark.headingColor;
    } else {
        borderColor = config.colors.borderColor;
        bodyBg = config.colors.bodyBg;
        headingColor = config.colors.headingColor;
    }

    // Variable declaration for table
    var dt_user_table = $('.datatables-users'),
        select2 = $('.select2'),
        userView = 'app-user-view-account.html',
        statusObj = {
            1: {title: 'Pending', class: 'bg-label-warning'},
            2: {title: 'Active', class: 'bg-label-success'},
            3: {title: 'Inactive', class: 'bg-label-secondary'}
        };

    if (select2.length) {
        var $this = select2;
        $this.wrap('<div class="position-relative"></div>').select2({
            placeholder: 'Select Country',
            dropdownParent: $this.parent()
        });
    }

    // Users datatable
    if (dt_user_table.length) {
        var dt_user = dt_user_table.DataTable({
            ajax: '/api/v1/users/?format=datatables', // JSON file to add data
            columns: [
                // columns according to JSON
                {
                    className: 'control',
                    searchable: false,
                    orderable: false,
                    responsivePriority: 2,
                    targets: 0,
                    data: '',
                    mRender: function (data, type, row) {
                        console.log(data)
                        return '';
                    }
                },
                {data: 'name'},
                {data: 'email'},
                {
                    data: 'group',
                    mRender: function (data, type, row) {
                        console.log(data)
                        if (data[0]) {
                            return data[0].name;
                        } else {
                            return '';
                        }
                    }
                },
                {data: 'company', name: 'profile.company.name'},
                {data: 'last_login'},
                {
                    data: 'is_active',
                    mRender: function (data, type, row) {
                        let statusObject = {
                            0: {title: 'Pending', class: 'bg-label-warning'},
                            1: {title: 'Active', class: 'bg-label-success'},
                            2: {title: 'Inactive', class: 'bg-label-secondary'},
                            true: {title: 'Active', class: 'bg-label-success'},
                            false: {title: 'Inactive', class: 'bg-label-secondary'},
                        };
                        var $status = data;
                        let tstatus = 0;
                        if (data == true) {
                            tstatus = 0;
                        } else {
                            tstatus = 1;
                        }

                        return (
                            `<span class="badge ${statusObject[$status].class} text-capitalized status_change" style="cursor: pointer;" title="Click Change Status" data-status="${tstatus}" data-id="${row.id}">${statusObject[$status].title}</span>`
                        );
                    }
                },
                {
                    data: null,
                    mRender: function (data, type, row) {
                        let userView = '';
                        // console.log(data);
                        let url = `/accounts/user/${data.id}/edit`;
                        // console.log(url);
                        let delete_button = '';
                        if (can_delete_user == 'True'){
                            delete_button = '<a href="javascript:;" class="text-body delete-record" data-id="' + data.id + '"><i class="ti ti-trash ti-sm mx-2"></i></a>';
                        }
                        return (
                            `<div class="d-flex align-items-center">
                                <a href="javascript:;"  data-url="${url}" class="text-body user-edit"><i class="ti ti-edit ti-sm me-2"></i></a>
                                ${delete_button}
                            </div>`
                        );
                    }
                }
            ],

            order: [[1, 'desc']],
            dom:
                '<"row me-2"' +
                '<"col-md-2"<"me-3"l>>' +
                '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                '>t' +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            language: {
                sLengthMenu: '_MENU_',
                search: '',
                searchPlaceholder: 'Search..'
            },
            // Buttons with Dropdown
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-label-secondary dropdown-toggle mx-3',
                    text: '<i class="ti ti-screen-share me-1 ti-xs"></i>Export',
                    buttons: [

                        {
                            extend: 'csv',
                            text: '<i class="ti ti-file-text me-2" ></i>CSV',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text: '<i class="ti ti-file-spreadsheet me-2"></i>EXCEL',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="ti ti-file-code-2 me-2"></i>PDF',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    text: '<i class="ti ti-plus me-0 me-sm-1 ti-xs"></i><span class="d-none d-sm-inline-block">Add New User</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'id': 'createUser',
                        'data-bs-toggle': 'offcanvas',
                        // 'data-bs-target': '#offcanvasAddUser'
                    }
                }
            ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/><tbody />').append(data) : false;
                    }
                }
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Role </option></select>'
                        )
                            .appendTo('.user_role')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>');
                            });
                    });
                // Adding plan filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserPlan" class="form-select text-capitalize"><option value=""> Select Plan </option></select>'
                        )
                            .appendTo('.user_plan')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         select.append('<option value="' + d + '">' + d + '</option>');
                        //     });
                    });
                // Adding status filter once table initialized
                this.api()
                    .columns(5)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="FilterTransaction" class="form-select text-capitalize"><option value=""> Select Status </option></select>'
                        )
                            .appendTo('.user_status')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         // select.append(
                        //         //     '<option value="' +
                        //         //     statusObj[d].title +
                        //         //     '" class="text-capitalize">' +
                        //         //     statusObj[d].title +
                        //         //     '</option>'
                        //         // );
                        //     });
                    });
            }
        });
    }

    // Delete Record
    $('.datatables-users tbody').on('click', '.delete-record', function () {
        let id = $(this).attr('data-id');
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        if (confirm('Are you sure !') == true) {
            axios.delete(`/api/v1/users/${id}/`)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
        }


        // dt_user.row($(this).parents('tr')).remove().draw();
    });

    $('html').on('click', '.status_change', function () {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let status = $(this).attr('data-status');
        let id = $(this).attr('data-id');
        axios.put(`/api/v1/users/${id}/`, {is_active: status})
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $('html').on('click', '.user-edit', function () {
        let url = $(this).attr('data-url');
        console.log(url)
        $('#ajaxModal').modal('show');
        $('#ajaxModal').find('#modal-content').load(url)
    });

    $('html').on('click', '#createUser', function () {
        let url = '/accounts/user/create'
        console.log(url)
        $('#ajaxModal').modal('show');
        $('#ajaxModal').find('#modal-content').load(url)
    });


    $('html').on('submit', '#editUserForm', function () {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        console.log(form_data);
        let id = $(this).attr('data-id');
        axios.put(`/api/v1/users/${id}/`, form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('#ajaxModal').find('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    $('html').on('submit', '#createUserForm', function () {
        // {#$('#user_creation_form_container').load('{% url "user_create" %}')#}
        let form_data = objectifyForm($('form#createUserForm').serializeArray());
        // {#const headers = {"X-CSRFTOKEN": "{% csrf_token() %}"}#}
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        axios.post("/api/v1/users/", form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error.response);
                if (error.response.status == 400) {
                    let em = error.response.data;
                    $('#createUserForm').find('.field_error').remove()
                    Object.entries(em).forEach(([key, value]) => {
                        console.log(`${key} ${value}`); // "a 5", "b 7", "c 9"
                        let fieldName = $('#createUserForm').find(`[name="${key}"]`);
                        if (key=='username'){
                            fieldName = $('#createUserForm').find(`[name="email"]`);
                        }
                        if (key=='password'){
                            fieldName = $('#createUserForm').find(`[name="password1"]`);
                        }

                        fieldName.closest('div').find('.field_error').remove()
                        fieldName.closest('div').append(`<span class="text-danger field_error">${value}</span>`)

                    });
                }

            });

    });

    $('[name="company"]').on('change', function () {
        let comapny = $(this).val();
        console.log(comapny)
        $('#user-role').find('option').prop('disabled', true).hide()
        $('#user-role').find('option[data-company-id="' + comapny + '"]').prop('disabled', false).show()
    })

    // Filter form control to default size
    // ? setTimeout used for multilingual table initialization
    setTimeout(() => {
        $('.dataTables_filter .form-control').removeClass('form-control-sm');
        $('.dataTables_length .form-select').removeClass('form-select-sm');
    }, 300);
});

// Validation & Phone mask
(function () {
    const phoneMaskList = document.querySelectorAll('.phone-mask'),
        addNewUserForm = document.getElementById('addNewUserForm');

    // Phone Number
    if (phoneMaskList) {
        phoneMaskList.forEach(function (phoneMask) {
            new Cleave(phoneMask, {
                phone: true,
                phoneRegionCode: 'US'
            });
        });
    }
    // Add New User Form Validation
    const fv = FormValidation.formValidation(addNewUserForm, {
        fields: {
            userFullname: {
                validators: {
                    notEmpty: {
                        message: 'Please enter fullname '
                    }
                }
            },
            userEmail: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({
                // Use this for enabling/changing valid/invalid class
                eleValidClass: '',
                rowSelector: function (field, ele) {
                    // field is the field name & ele is the field element
                    return '.mb-3';
                }
            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
})();
