let cardColor, headingColor, labelColor, shadeColor, grayColor,borderColor,legendColor;
if (isDarkStyle) {
    cardColor = config.colors_dark.cardColor;
    labelColor = config.colors_dark.textMuted;
    headingColor = config.colors_dark.headingColor;
    shadeColor = 'dark';
    grayColor = '#5E6692'; // gray color is for stacked bar chart
    borderColor = config.colors_dark.borderColor;
    legendColor = config.colors_dark.bodyColor;
} else {
    cardColor = config.colors.cardColor;
    labelColor = config.colors.textMuted;
    headingColor = config.colors.headingColor;
    shadeColor = '';
    grayColor = '#817D8D';
    borderColor = config.colors.borderColor;
    legendColor = config.colors.bodyColor;
}
// Revenue CLICK Generated Area Chart
// --------------------------------------------------------------------
const revenueGeneratedE3 = document.querySelector('#revenueGeneratedR'),
    revenueGeneratedRConfig = {
        chart: {
            height: 130,
            type: 'area',
            parentHeightOffset: 0,
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true
            }
        },
        markers: {
            colors: 'transparent',
            strokeColors: 'transparent'
        },
        grid: {
            show: false
        },
        colors: [config.colors.danger],
        fill: {
            type: 'gradient',
            gradient: {
                shade: shadeColor,
                shadeIntensity: 0.8,
                opacityFrom: 0.6,
                opacityTo: 0.1
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 2,
            curve: 'smooth'
        },
        series: [
            {
                data: [0, 0]
            }
        ],
        xaxis: {
            show: true,
            lines: {
                show: false
            },
            labels: {
                show: false
            },
            stroke: {
                width: 0
            },
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            stroke: {
                width: 0
            },
            show: false
        },
        tooltip: {
            enabled: false
        }
    };
if (typeof revenueGeneratedE3 !== undefined && revenueGeneratedE3 !== null) {
    var revenueGeneratedR = new ApexCharts(revenueGeneratedE3, revenueGeneratedRConfig);
    revenueGeneratedR.render();
}

// Revenue CLICK Generated Area Chart
// --------------------------------------------------------------------
const revenueGeneratedE2 = document.querySelector('#revenueGeneratedX'),
    revenueGeneratedXConfig = {
        chart: {
            height: 130,
            type: 'area',
            parentHeightOffset: 0,
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true
            }
        },
        markers: {
            colors: 'transparent',
            strokeColors: 'transparent'
        },
        grid: {
            show: false
        },
        colors: [config.colors.info],
        fill: {
            type: 'gradient',
            gradient: {
                shade: shadeColor,
                shadeIntensity: 0.8,
                opacityFrom: 0.6,
                opacityTo: 0.1
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 2,
            curve: 'smooth'
        },
        series: [
            {
                data: [0,0]
            }
        ],
        xaxis: {
            show: true,
            lines: {
                show: false
            },
            labels: {
                show: false
            },
            stroke: {
                width: 0
            },
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            stroke: {
                width: 0
            },
            show: false
        },
        tooltip: {
            enabled: false
        }
    };
if (typeof revenueGeneratedE2 !== undefined && revenueGeneratedE2 !== null) {
    var revenueGeneratedX = new ApexCharts(revenueGeneratedE2, revenueGeneratedXConfig);
    revenueGeneratedX.render();
}


// Revenue Generated Area Chart
// --------------------------------------------------------------------
const revenueGeneratedEl = document.querySelector('#revenueGenerated'),
    revenueGeneratedConfig = {
        chart: {
            height: 130,
            type: 'area',
            parentHeightOffset: 0,
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true
            }
        },
        markers: {
            colors: 'transparent',
            strokeColors: 'transparent'
        },
        grid: {
            show: false
        },
        colors: [config.colors.success],
        fill: {
            type: 'gradient',
            gradient: {
                shade: shadeColor,
                shadeIntensity: 0.8,
                opacityFrom: 0.6,
                opacityTo: 0.1
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 2,
            curve: 'smooth'
        },
        series: [
            {
                data: [0,0]
            }
        ],
        xaxis: {
            show: true,
            lines: {
                show: false
            },
            labels: {
                show: false
            },
            stroke: {
                width: 0
            },
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            stroke: {
                width: 0
            },
            show: false
        },
        tooltip: {
            enabled: false
        }
    };
if (typeof revenueGeneratedEl !== undefined && revenueGeneratedEl !== null) {
    var revenueGenerated = new ApexCharts(revenueGeneratedEl, revenueGeneratedConfig);
    revenueGenerated.render();
}



  const lineChart = document.getElementById('lineChart');
  if (lineChart) {
    var lineChartVar = new Chart(lineChart, {
      type: 'line',
      data: {
        labels: [1, 2, 3, 4, 5, 6],
        datasets: [
          {
            data: [80, 150, 180, 270, 210,370],
            label: 'Impression',
            borderColor: config.colors.danger,
            tension: 0.5,
            pointStyle: 'circle',
            backgroundColor: config.colors.danger,
            fill: false,
            pointRadius: 1,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBorderColor: cardColor,
            pointHoverBackgroundColor: config.colors.danger
          },
            {
            data: [80, 125, 105, 130, 215,290],
            label: 'Visible',
            borderColor: config.colors.primary,
            tension: 0.5,
            pointStyle: 'circle',
            backgroundColor: config.colors.primary,
            fill: false,
            pointRadius: 1,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBorderColor: cardColor,
            pointHoverBackgroundColor: config.colors.primary
          },
            {
            data: [80, 150, 180, 270, 210,370],
            label: 'Clicks',
            borderColor: config.colors.info,
            tension: 0.5,
            pointStyle: 'circle',
            backgroundColor: config.colors.info,
            fill: false,
            pointRadius: 1,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBorderColor: cardColor,
            pointHoverBackgroundColor: config.colors.info
          }

          // {
          //   data: [80, 99, 82, 90, 115, 115, 74, 75, 130, 155, 125, 90, 140, 130, 180],
          //   label: 'Africa',
          //   borderColor: yellowColor,
          //   tension: 0.5,
          //   pointStyle: 'circle',
          //   backgroundColor: yellowColor,
          //   fill: false,
          //   pointRadius: 1,
          //   pointHoverRadius: 5,
          //   pointHoverBorderWidth: 5,
          //   pointBorderColor: 'transparent',
          //   pointHoverBorderColor: cardColor,
          //   pointHoverBackgroundColor: yellowColor
          // }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          x: {
            grid: {
              color: borderColor,
              drawBorder: false,
              borderColor: borderColor
            },
            ticks: {
              color: labelColor
            }
          },
          y: {
            scaleLabel: {
              display: true
            },
            // min: 0,
            // max: 400,
            ticks: {
              color: labelColor,
              // stepSize: 10000
            },
            grid: {
              color: borderColor,
              drawBorder: false,
              borderColor: borderColor
            }
          }
        },
        plugins: {
          tooltip: {
            // Updated default tooltip UI
            rtl: isRtl,
            backgroundColor: cardColor,
            titleColor: headingColor,
            bodyColor: legendColor,
            borderWidth: 1,
            borderColor: borderColor
          },
          legend: {
            position: 'top',
            align: 'start',
            rtl: isRtl,
            labels: {
              usePointStyle: true,
              padding: 35,
              boxWidth: 6,
              boxHeight: 6,
              color: legendColor
            }
          }
        }
      }
    });
  }


  const lineChartC = document.getElementById('lineChartC');
  if (lineChartC) {
    var lineChartCVar = new Chart(lineChartC, {
      type: 'line',
      data: {
        labels: [1, 2, 3, 4, 5, 6],
        datasets: [
          {
            data: [80, 150, 180, 270, 210,370],
            label: 'Clicks',
            borderColor: config.colors.info,
            tension: 0.5,
            pointStyle: 'circle',
            backgroundColor: config.colors.info,
            fill: false,
            pointRadius: 1,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 5,
            pointBorderColor: 'transparent',
            pointHoverBorderColor: cardColor,
            pointHoverBackgroundColor: config.colors.info
          }

        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          x: {
            grid: {
              color: borderColor,
              drawBorder: false,
              borderColor: borderColor
            },
            ticks: {
              color: labelColor
            }
          },
          y: {
            scaleLabel: {
              display: true
            },
            // min: 0,
            // max: 400,
            ticks: {
              color: labelColor,
              // stepSize: 10000
            },
            grid: {
              color: borderColor,
              drawBorder: false,
              borderColor: borderColor
            }
          }
        },
        plugins: {
          tooltip: {
            // Updated default tooltip UI
            rtl: isRtl,
            backgroundColor: cardColor,
            titleColor: headingColor,
            bodyColor: legendColor,
            borderWidth: 1,
            borderColor: borderColor
          },
          legend: {
            position: 'top',
            align: 'start',
            rtl: isRtl,
            labels: {
              usePointStyle: true,
              padding: 35,
              boxWidth: 6,
              boxHeight: 6,
              color: legendColor
            }
          }
        }
      }
    });
  }
