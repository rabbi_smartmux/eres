/**
 * Page User List
 */

'use strict';

// Datatable (jquery)
$(function () {
    let borderColor, bodyBg, headingColor;

    if (isDarkStyle) {
        borderColor = config.colors_dark.borderColor;
        bodyBg = config.colors_dark.bodyBg;
        headingColor = config.colors_dark.headingColor;
    } else {
        borderColor = config.colors.borderColor;
        bodyBg = config.colors.bodyBg;
        headingColor = config.colors.headingColor;
    }

    // Variable declaration for table
    var dt_user_table = $('.datatables-users'),
        select2 = $('.select2'),
        userView = 'app-user-view-account.html',
        statusObj = {
            1: {title: 'Pending', class: 'bg-label-warning'},
            2: {title: 'Active', class: 'bg-label-success'},
            3: {title: 'Inactive', class: 'bg-label-secondary'}
        };

    if (select2.length) {
        var $this = select2;
        $this.wrap('<div class="position-relative"></div>').select2({
            placeholder: 'Select Country',
            dropdownParent: $this.parent()
        });
    }

    // Users datatable
    if (dt_user_table.length) {
        var dt_user = dt_user_table.DataTable({
            ajax: '/api/v1/useragent/?format=datatables', // JSON file to add data
            columns: [
                // columns according to JSON
                {
                    data: '',
                    mRender: function (data, type, row) {
                        console.log(data)
                        return '';
                    }
                },
                {data: 'uag'},
                {data: 'details'},
                {
                    data: null,
                    mRender: function (data, type, row) {
                        let userView = '';
                        console.log(data);
                        let url = `/settings/useragent/${data.id}/edit`;
                        console.log(url);
                        return (
                            '<div class="d-flex align-items-center">' +
                            '<a href="javascript:;"  data-url="' + url + '" class="text-body user-edit" data-id="' + data.id + '"><i class="ti ti-edit ti-sm me-2"></i></a>' +
                            '<a href="javascript:;" class="text-body delete-record" data-id="' + data.id + '"><i class="ti ti-trash ti-sm mx-2"></i></a>' +
                            // '<a href="javascript:;" class="text-body dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical ti-sm mx-1"></i></a>' +
                            // '<div class="dropdown-menu dropdown-menu-end m-0">' +
                            // '<a href="' +
                            // userView +
                            // '" class="dropdown-item">View</a>' +
                            // '<a href="javascript:;" class="dropdown-item">Suspend</a>' +
                            // '</div>' +
                            '</div>'
                        );
                    }
                }
            ],
            // columnDefs: [
            //   {
            //     // For Responsive
            //     className: 'control',
            //     searchable: false,
            //     orderable: false,
            //     responsivePriority: 2,
            //     targets: 0,
            //     render: function (data, type, full, meta) {
            //       return '';
            //     }
            //   },
            //   {
            //     // User full name and email
            //     targets: 1,
            //     responsivePriority: 4,
            //     render: function (data, type, full, meta) {
            //       $row_output = full;
            //       return $row_output;
            //     }
            //   },
            //   {
            //     // User Role
            //     targets: 2,
            //     render: function (data, type, full, meta) {
            //       var $role = full['role'];
            //       var roleBadgeObj = {
            //         Subscriber:
            //           '<span class="badge badge-center rounded-pill bg-label-warning w-px-30 h-px-30 me-2"><i class="ti ti-user ti-sm"></i></span>',
            //         Author:
            //           '<span class="badge badge-center rounded-pill bg-label-success w-px-30 h-px-30 me-2"><i class="ti ti-circle-check ti-sm"></i></span>',
            //         Maintainer:
            //           '<span class="badge badge-center rounded-pill bg-label-primary w-px-30 h-px-30 me-2"><i class="ti ti-chart-pie-2 ti-sm"></i></span>',
            //         Editor:
            //           '<span class="badge badge-center rounded-pill bg-label-info w-px-30 h-px-30 me-2"><i class="ti ti-edit ti-sm"></i></span>',
            //         Admin:
            //           '<span class="badge badge-center rounded-pill bg-label-secondary w-px-30 h-px-30 me-2"><i class="ti ti-device-laptop ti-sm"></i></span>'
            //       };
            //       return "<span class='text-truncate d-flex align-items-center'>" + roleBadgeObj[$role] + $role + '</span>';
            //     }
            //   },
            //   {
            //     // Plans
            //     targets: 3,
            //     render: function (data, type, full, meta) {
            //       var $plan = full['current_plan'];
            //
            //       return '<span class="fw-medium">' + $plan + '</span>';
            //     }
            //   },
            //   {
            //     // User Status
            //     targets: 5,
            //     render: function (data, type, full, meta) {
            //       var $status = full['status'];
            //
            //       return (
            //         '<span class="badge ' +
            //         statusObj[$status].class +
            //         '" text-capitalized>' +
            //         statusObj[$status].title +
            //         '</span>'
            //       );
            //     }
            //   },
            //   {
            //     // Actions
            //     targets: -1,
            //     title: 'Actions',
            //     searchable: false,
            //     orderable: false,
            //     render: function (data, type, full, meta) {
            //       return (
            //         '<div class="d-flex align-items-center">' +
            //         '<a href="javascript:;" class="text-body"><i class="ti ti-edit ti-sm me-2"></i></a>' +
            //         '<a href="javascript:;" class="text-body delete-record"><i class="ti ti-trash ti-sm mx-2"></i></a>' +
            //         '<a href="javascript:;" class="text-body dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="ti ti-dots-vertical ti-sm mx-1"></i></a>' +
            //         '<div class="dropdown-menu dropdown-menu-end m-0">' +
            //         '<a href="' +
            //         userView +
            //         '" class="dropdown-item">View</a>' +
            //         '<a href="javascript:;" class="dropdown-item">Suspend</a>' +
            //         '</div>' +
            //         '</div>'
            //       );
            //     }
            //   }
            // ],
            order: [[1, 'desc']],
            dom:
                '<"row me-2"' +
                '<"col-md-2"<"me-3"l>>' +
                '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
                '>t' +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                '>',
            language: {
                sLengthMenu: '_MENU_',
                search: '',
                searchPlaceholder: 'Search..'
            },
            // Buttons with Dropdown
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-label-secondary dropdown-toggle mx-3',
                    text: '<i class="ti ti-screen-share me-1 ti-xs"></i>Export',
                    buttons: [
                        {
                            extend: 'print',
                            text: '<i class="ti ti-printer me-2" ></i>Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be print
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            },
                            customize: function (win) {
                                //customize print view for dark
                                $(win.document.body)
                                    .css('color', headingColor)
                                    .css('border-color', borderColor)
                                    .css('background-color', bodyBg);
                                $(win.document.body)
                                    .find('table')
                                    .addClass('compact')
                                    .css('color', 'inherit')
                                    .css('border-color', 'inherit')
                                    .css('background-color', 'inherit');
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<i class="ti ti-file-text me-2" ></i>Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text: '<i class="ti ti-file-spreadsheet me-2"></i>Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="ti ti-file-code-2 me-2"></i>Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'copy',
                            text: '<i class="ti ti-copy me-2" ></i>Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    text: '<i class="ti ti-plus me-0 me-sm-1 ti-xs"></i><span class="d-none d-sm-inline-block">Add New Handset OS</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'id': 'create_useragent',
                        // 'data-bs-toggle': 'offcanvas',
                        // 'data-bs-target': '#offcanvasAddUser'
                    }
                }
            ],
            // For responsive popup
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Details of ' + data['full_name'];
                        }
                    }),
                    type: 'column',
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                                ? '<tr data-dt-row="' +
                                col.rowIndex +
                                '" data-dt-column="' +
                                col.columnIndex +
                                '">' +
                                '<td>' +
                                col.title +
                                ':' +
                                '</td> ' +
                                '<td>' +
                                col.data +
                                '</td>' +
                                '</tr>'
                                : '';
                        }).join('');

                        return data ? $('<table class="table"/><tbody />').append(data) : false;
                    }
                }
            },
            initComplete: function () {
                // Adding role filter once table initialized
                this.api()
                    .columns(3)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="UserRole" class="form-select text-capitalize"><option value=""> Select Type </option></select>'
                        )
                            .appendTo('.user_role')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         select.append('<option value="' + d + '">' + d + '</option>');
                        //     });
                    });
                // Adding plan filter once table initialized
                // this.api()
                //     .columns(3)
                //     .every(function () {
                //         var column = this;
                //         var select = $(
                //             '<select id="UserPlan" class="form-select text-capitalize"><option value=""> Select Plan </option></select>'
                //         )
                //             .appendTo('.user_plan')
                //             .on('change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex($(this).val());
                //                 column.search(val ? '^' + val + '$' : '', true, false).draw();
                //             });
                //
                //         // column
                //         //     .data()
                //         //     .unique()
                //         //     .sort()
                //         //     .each(function (d, j) {
                //         //         select.append('<option value="' + d + '">' + d + '</option>');
                //         //     });
                //     });
                // Adding status filter once table initialized
                this.api()
                    .columns(5)
                    .every(function () {
                        var column = this;
                        var select = $(
                            '<select id="FilterTransaction" class="form-select text-capitalize"><option value=""> Select Status </option></select>'
                        )
                            .appendTo('.user_status')
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });

                        // column
                        //     .data()
                        //     .unique()
                        //     .sort()
                        //     .each(function (d, j) {
                        //         // select.append(
                        //         //     '<option value="' +
                        //         //     statusObj[d].title +
                        //         //     '" class="text-capitalize">' +
                        //         //     statusObj[d].title +
                        //         //     '</option>'
                        //         // );
                        //     });
                    });
            }
        });
    }

    // Delete Record
    $('.datatables-users tbody').on('click', '.delete-record', function () {
        let id = $(this).attr('data-id');
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        if (confirm('Are you sure !') == true) {
            axios.delete(`/api/v1/useragent/${id}/`)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
        }



        // dt_user.row($(this).parents('tr')).remove().draw();
    });

    $('html').on('click', '.status_change', function () {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let status = $(this).attr('data-status');
        let id = $(this).attr('data-id');
        axios.put(`/api/v1/useragent/${id}/`, {status: status})
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
    })

    $('html').on('click', '#create_useragent', function () {
        let url = '/settings/useragent/create';
        console.log(url)
        $('#ajaxModal').modal('show');
        $('#ajaxModal').find('#modal-content').load(url,function () {
            $('#ajaxModal').find('form').attr('action',url)
            $('#ajaxModal').find('#close_modal').attr('data-bs-dismiss','modal')
            $('#ajaxModal').find('#close_modal').attr('aria-label',"Close")
        })

    });

    $('html').on('click', '.user-edit', function () {
        let url = $(this).attr('data-url');
        let id = $(this).attr('data-id');
        console.log(url)
        $('#ajaxModal').modal('show');
        $('#ajaxModal').find('#modal-content').load(url,function () {
            $('#ajaxModal').find('form').attr('action',url)
            $('#ajaxModal').find('form').attr('data-id',id)
            $('#ajaxModal').find('#close_modal').attr('data-bs-dismiss','modal')
            $('#ajaxModal').find('#close_modal').attr('aria-label',"Close")
            // let details = $('#ajaxModal').find('[name="details"]').text()
            // console.log(details)
            // console.log(JSON.parse(details))
            // $('#ajaxModal').find('[name="details"]').text(JSON.parse(details));

        })

    });



    $('html').on('submit', '#country_form', function (e) {
        e.preventDefault();
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        let form_data = objectifyForm($(this).serializeArray());
        console.log(form_data);
        let id = $(this).attr('data-id');
        if (id){
            axios.put(`/api/v1/useragent/${id}/`, form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('#ajaxModal').find('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error);
            });

        }else {
            axios.post(`/api/v1/useragent/`, form_data)
            .then(function (response) {
                console.log(response);
                dt_user.ajax.reload();
                $('#ajaxModal').find('[aria-label="Close"]').click()
            })
            .catch(function (error) {
                console.log(error);
            });
        }

    });
    $('html').on('click', '#submit_data', function () {
            // {#$('#user_creation_form_container').load('{% url "user_create" %}')#}
            let form_data = objectifyForm($('form#addNewUserForm').serializeArray());
            // {#const headers = {"X-CSRFTOKEN": "{% csrf_token() %}"}#}
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
            axios.defaults.xsrfCookieName = "csrftoken";
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }

            axios.post("/api/v1/useragent/", form_data)
                .then(function (response) {
                    console.log(response);
                    dt_user.ajax.reload();
                    $('html').find('[aria-label="Close"]').click();
                })
                .catch(function (error) {
                    console.log(error);
                });

        });

    // Filter form control to default size
    // ? setTimeout used for multilingual table initialization
    setTimeout(() => {
        $('.dataTables_filter .form-control').removeClass('form-control-sm');
        $('.dataTables_length .form-select').removeClass('form-select-sm');
    }, 300);
});

// Validation & Phone mask
(function () {
    const phoneMaskList = document.querySelectorAll('.phone-mask'),
        addNewUserForm = document.getElementById('addNewUserForm');

    // Phone Number
    if (phoneMaskList) {
        phoneMaskList.forEach(function (phoneMask) {
            new Cleave(phoneMask, {
                phone: true,
                phoneRegionCode: 'US'
            });
        });
    }
    // Add New User Form Validation
    const fv = FormValidation.formValidation(addNewUserForm, {
        fields: {
            userFullname: {
                validators: {
                    notEmpty: {
                        message: 'Please enter fullname '
                    }
                }
            },
            userEmail: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email'
                    },
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            bootstrap5: new FormValidation.plugins.Bootstrap5({
                // Use this for enabling/changing valid/invalid class
                eleValidClass: '',
                rowSelector: function (field, ele) {
                    // field is the field name & ele is the field element
                    return '.mb-3';
                }
            }),
            submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
            // defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            autoFocus: new FormValidation.plugins.AutoFocus()
        }
    });
})();
